//
//
//    Create by imac on 29/4/2018
//    Copyright © 2018. All rights reserved.

import Foundation
import UIKit
// MARK: - ...  UserAuth Controller
class UserRoot: Codable {
    // MARK: - ...  Keys UserDefaults
    public static var storeUserDefaults: String = "userDataDefaults"
    public static var storeRememberUser: String = "USER_LOGIN_REMEMBER"
    public static var storeTimeStamp: String = "LOGIN_TIMESTAMP"

    var data: User?
    var expire: Int?
    var refresh_token: String?
    var loginTimeStamp: Int?
}
// MARK: - ...  User Codable from API
extension UserRoot {
    class User: Codable {
        let id: Int?
        let name, email, phone, gender: String?
        let birthdate, address, lat, lng: String?
        let avatar: String?
        let active, confirmationCode, rating: Int?
        let userType: String?
        let attributes: Attributes?
        let categories, images: [Category]?
        let features: [Feature]?
        let token: String?
        let ordersCount: Int?
        let reservationsCount: Int?
        enum CodingKeys: String, CodingKey {
            case id, name, email, phone, gender, birthdate, address, lat, lng, avatar, active
            case confirmationCode = "confirmation_code"
            case rating
            case userType = "user_type"
            case attributes, categories, images, token
            case features
            case ordersCount = "orders_count"
            case reservationsCount = "reservations_count"
        }
    }
    
    // MARK: - Attributes
    struct Attributes: Codable {
        let id, userID, providerID: Int?
        let type: String?
        let nationalityID: Int?
        let providerName, workHours, from, to: String?
        let minimumOrderPrice, freeOrderPrice, detailsAr, detailsEn: String?
        let socailAccount1, socailAccount2, bankName, iban: String?
        let commercialRegister, commercialImg, receiptImg: String?
        let mix: Int?
        let meatType, meatSource, otherMeatSource: String?
        let deliveryOption, tableOption, priceAdded: Int?
        let createdAt, updatedAt: String?
        let nationality: Nationality?

        enum CodingKeys: String, CodingKey {
            case id
            case userID = "user_id"
            case providerID = "provider_id"
            case type
            case nationalityID = "nationality_id"
            case providerName = "provider_name"
            case workHours = "work_hours"
            case from, to
            case minimumOrderPrice = "minimum_order_price"
            case freeOrderPrice = "free_order_price"
            case detailsAr = "details_ar"
            case detailsEn = "details_en"
            case socailAccount1 = "socail_account1"
            case socailAccount2 = "socail_account2"
            case bankName = "bank_name"
            case iban
            case commercialRegister = "commercial_register"
            case commercialImg = "commercial_img"
            case receiptImg = "receipt_img"
            case mix
            case meatType = "meat_type"
            case meatSource = "meat_source"
            case otherMeatSource = "other_meat_source"
            case deliveryOption = "delivery_option"
            case tableOption = "table_option"
            case priceAdded = "price_added"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case nationality

        }
    }
    // MARK: - Feature
    struct Feature: Codable {
        let id, featureID, userID: Int?
        let createdAt, updatedAt: String?
        let feature: Nationality?
        
        enum CodingKeys: String, CodingKey {
            case id
            case featureID = "feature_id"
            case userID = "user_id"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case feature
        }
    }
    // MARK: - Nationality
    struct Nationality: Codable {
        let id: Int?
        let nameAr, nameEn: String?
        let active: Int?
        let createdAt, updatedAt: String?
        let img: String?
        
        var name: String? {
            if Localizer.current == .arabic {
                return nameAr
            } else {
                return nameEn
            }
        }
        enum CodingKeys: String, CodingKey {
            case id
            case nameAr = "name_ar"
            case nameEn = "name_en"
            case active
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case img
        }
    }
    // MARK: - Category
    struct Category: Codable {
        let id, categoryID, userID: Int?
        let createdAt, updatedAt: String?
        let name: String?
        let img: String?
        let category: Nationality?

        enum CodingKeys: String, CodingKey {
            case id
            case categoryID = "category_id"
            case userID = "user_id"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case name
            case img
            case category
        }
        
        static func transform(for list: [Category]) -> [TagModel] {
            var array: [ServiceTypeModel.Datum] = []
            for item in list {
                let model = ServiceTypeModel.Datum(id: item.categoryID, img: nil, name: item.category?.name, active: 1)
                array.append(model)
            }
            return array
        }
    }

}
// MARK: - ...  Functions
extension UserRoot {
    // MARK: - ...  Function for convert data to model
    public static func convertToModel(response: Data?) -> UserRoot {
        do {
            let data = try JSONDecoder().decode(self, from: response ?? Data())
            return data
        } catch {
            return UserRoot()
        }
    }
    // MARK: - ...  Function for Save user data
    public static func save(response: Data?, remember: Bool = true) {
        let timestamp = NSDate().timeIntervalSince1970
        let myTimeInterval = TimeInterval(timestamp).int
        UserDefaults.standard.set(response, forKey: storeUserDefaults)
        UserDefaults.standard.set(myTimeInterval, forKey: storeTimeStamp)
        if remember {
            UserDefaults.standard.set(true, forKey: storeRememberUser)
        }
    }
    // MARK: - ...  Function for Save user data
    public func save() {
        guard let response = try? JSONEncoder().encode(self) else { return }
        UserDefaults.standard.set(response, forKey: UserRoot.storeUserDefaults)
    }
    // MARK: - ...  Function for fetch user data
    public static func fetch() -> UserRoot? {
        let data = UserDefaults.standard.data(forKey: storeUserDefaults)
        let user = self.convertToModel(response: data)
        return user
    }
    // MARK: - ...  Function for fetch user data
    public static func fetchUser() -> UserRoot.User? {
        let data = UserDefaults.standard.data(forKey: storeUserDefaults)
        let user = self.convertToModel(response: data)
        return user.data
    }
    // MARK: - ...  Function for fetch token 
    public static func token() -> String? {
        let data = UserDefaults.standard.data(forKey: storeUserDefaults)
        let user = self.convertToModel(response: data)
        return user.data?.token
    }
    public static func authroize() {
        if token() == nil {
            let scene = UIApplication.topViewController() as? BaseController
            PopupConfirmationVC.confirmationPOPUP(view: scene, title: "You must be logged in first !".localized, btns: [.agree, .skip]) {
                return
            } onAgreeClosure: {
                Router.instance.unAuthorized()
                return
            }
            return
        }
    }

}

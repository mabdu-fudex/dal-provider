//
//  Router.swift
//  BaseIOS
//
//  Created by M.abdu on 10/12/20.
//  Copyright © 2020 com.M.abdu. All rights reserved.
//

import Foundation
import UIKit
// MARK: - ...  Router for All Application
class Router: NSObject {
    struct Static {
        static var instance: Router?
    }
    class var instance: Router {
        if Static.instance == nil {
            Static.instance = Router()
        }
        return Static.instance!
    }
    let storyboard = UIStoryboard(name: "LangIntroStoryboard", bundle: nil)
    // MARK: - ...  Restart the main storyboard
    func restart(storyboard: UIStoryboard = Router.instance.storyboard) {
        if UserRoot.token() == nil {
            self.unAuthorized()
        } else {
            guard let scene = R.storyboard.homeStoryboard().instantiateInitialViewController() else { return }
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = scene
        }
        
    }
    // MARK: - ...  Restart the auth storyboard
    func unAuthorized(storyboard: UIStoryboard = Router.instance.storyboard) {
        if Localizer.isChanged {
            let scene = R.storyboard.loginStoryboard().instantiateInitialViewController()
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = scene
        } else {
            let scene = storyboard.instantiateInitialViewController()
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = scene
        }
    }
}

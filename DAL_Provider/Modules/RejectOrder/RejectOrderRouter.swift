//
//  RejectOrderRouter.swift
//  DAL_Provider
//
//  Created by Mabdu on 28/02/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class RejectOrderRouter: Router {
    typealias PresentingView = RejectOrderVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension RejectOrderRouter: RejectOrderRouterContract {
    func didDismiss() {
        view?.dismiss(animated: true, completion: nil)
    }
    func didReject() {
       
        view?.dismiss(animated: true, completion: {
            if self.view?.type == .reservation {
                self.view?.delegate?.rejectOrder(self.view?.delegate, reservation: self.view?.reservation)
            } else {
                self.view?.delegate?.rejectOrder(self.view?.delegate, order: self.view?.order)
            }
        })
    }
}

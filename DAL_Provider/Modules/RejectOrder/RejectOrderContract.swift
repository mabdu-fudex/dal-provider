//
//  RejectOrderContract.swift
//  DAL_Provider
//
//  Created by Mabdu on 28/02/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol RejectOrderPresenterContract: PresenterProtocol {
    func reject(type: OrderType, id: Int?, reason: String?)
}
// MARK: - ...  View Contract
protocol RejectOrderViewContract: PresentingViewProtocol {
    func didReject()
}
// MARK: - ...  Router Contract
protocol RejectOrderRouterContract: Router, RouterProtocol {
    func didDismiss()
    func didReject()
}

protocol RejectOrderDelegate: class {
    func rejectOrder(_ delegate: RejectOrderDelegate?, order: OrdersModel.Datum?)
    func rejectOrder(_ delegate: RejectOrderDelegate?, reservation: ReservationsModel.Datum?)
}

extension RejectOrderDelegate {
    func rejectOrder(_ delegate: RejectOrderDelegate?, order: OrdersModel.Datum?) {
        
    }
    func rejectOrder(_ delegate: RejectOrderDelegate?, reservation: ReservationsModel.Datum?) {
        
    }
}

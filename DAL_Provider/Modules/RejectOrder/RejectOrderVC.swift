//
//  RejectOrderVC.swift
//  DAL_Provider
//
//  Created by Mabdu on 28/02/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class RejectOrderVC: BaseController {
    @IBOutlet weak var rejectLbl: UILabel!
    @IBOutlet weak var reasonTxv: UITextView!
    var presenter: RejectOrderPresenter?
    var router: RejectOrderRouter?
    var type: OrderType = .order
    var order: OrdersModel.Datum?
    var reservation: ReservationsModel.Datum?
    weak var delegate: RejectOrderDelegate?
}

// MARK: - ...  LifeCycle
extension RejectOrderVC {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension RejectOrderVC {
    func setup() {
        let username = reservation?.userName ?? order?.userName
        rejectLbl.text = "\("We will send a notification to the".localized) \(username ?? ""), \("please explain the reason for rejection".localized)"
    }
    override func backBtn(_ sender: Any) {
        router?.didDismiss()
    }
    @IBAction func send(_ sender: Any) {
        reasonTxv.endEditing(true)
        if reasonTxv.text == reasonTxv.localization {
            reasonTxv.textColor = .red
            return
        }
        startLoading()
        presenter?.reject(type: type, id: reservation?.id ?? order?.id, reason: reasonTxv.text)
    }
    @IBAction func reject(_ sender: Any) {
        router?.didDismiss()
    }
}
// MARK: - ...  View Contract
extension RejectOrderVC: RejectOrderViewContract {
    func didReject() {
        stopLoading()
        router?.didReject()
    }
}

//
//  RejectOrderPresenter.swift
//  DAL_Provider
//
//  Created by Mabdu on 28/02/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class RejectOrderPresenter: BasePresenter<RejectOrderViewContract> {
}
// MARK: - ...  Presenter Contract
extension RejectOrderPresenter: RejectOrderPresenterContract {
    func reject(type: OrderType, id: Int?, reason: String?) {
        var method = ""
        if type == .reservation {
            method = NetworkConfigration.EndPoint.refuseReservation.rawValue
            NetworkManager.instance.paramaters["reservation_id"] = id
        } else {
            method = NetworkConfigration.EndPoint.refuseOrder.rawValue
            NetworkManager.instance.paramaters["order_id"] = id
        }
        NetworkManager.instance.paramaters["refuseal"] = reason
        NetworkManager.instance.request(method, type: .post, RejectOrderModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didReject()
                case .failure:
                    break
            }
        }
    }
}
// MARK: - ...  Example of network response


//
//  ReservationsContract.swift
//  DAL_Provider
//
//  Created by Mabdu on 28/02/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol ReservationsPresenterContract: PresenterProtocol {
    func fetchReservations(tab: OrdersVC.Tab)
    func filterReservations(filter: ReservationsFilterModel?)
    func finishReservation(for id: Int?)
}
// MARK: - ...  View Contract
protocol ReservationsViewContract: PresentingViewProtocol {
    func didFetch(for list: [ReservationsModel.Datum]?)
    func didFinishReservation(for id: Int?)
}
// MARK: - ...  Router Contract
protocol ReservationsRouterContract: Router, RouterProtocol {
    func filter()
    func reservation(for reservation: ReservationsModel.Datum?)
    func acceptReservation(for reservation: ReservationsModel.Datum?)
    func rejectReservation(for reservation: ReservationsModel.Datum?)
}

//
//  OrderDetailsVC.swift
//  DAL_IOS
//
//  Created by M.abdu on 2/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit
import Cosmos
// MARK: - ...  ViewController - Vars
class ReservationDetailsVC: BaseController {
    @IBOutlet weak var centerTypeConstraint: NSLayoutConstraint!
    @IBOutlet weak var orderTypeLbl: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var personsLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var serviceLbl: UILabel!
    @IBOutlet weak var noteLbl: UILabel!
    @IBOutlet weak var statusBtn: GradientButton!
    @IBOutlet weak var newOrderView: UIView!
    var presenter: ReservationDetailsPresenter?
    var router: ReservationDetailsRouter?
    var tab: OrdersVC.Tab = .new
    var reservation: ReservationsModel.Datum?
    weak var delegate: ReservationDetailsDelegate?
}

// MARK: - ...  LifeCycle
extension ReservationDetailsVC {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
        setupUI()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension ReservationDetailsVC {
    func setup() {
        if Localizer.current == .arabic {
            centerTypeConstraint.constant = 70
        } else {
            centerTypeConstraint.constant = -70
        }
        if tab == .new {
            setupUIForNewOrder()
        } else {
            setupUIForCompleteOrder()
        }
    }
    func setupUI() {
        orderTypeLbl.text = "\("Order type".localized)  \("book table".localized)"
        orderTypeLbl.attributedText = orderTypeLbl.text?.colorOfWord("\("book table".localized)", with: R.color.secondColor() ?? .black)
        userImage.setImage(url: reservation?.userImg)
        usernameLbl.text = reservation?.userName
        personsLbl.text = "\(reservation?.qty ?? 0) \("persons".localized)"
        let dateDay = DateHelper().date(date: reservation?.reservationDate, format: "d MMM yyyy", oldFormat: "yyyy-MM-dd")
        let dateHour = DateHelper().date(date: reservation?.reservationTime, format: "h:mm a", oldFormat: "HH:mm:ss")
        dateLbl.text = "\(dateDay ?? "") , \(dateHour ?? "")"
        serviceLbl.text = reservation?.featureName
        noteLbl.text = ""
        statusBtn.setTitle("\(reservation?.statusText ?? "")", for: .normal)
    }
    func setupUIForNewOrder() {

    }
    func setupUIForCompleteOrder() {
        newOrderView.isHidden = true
        statusBtn.firstColor = R.color.textColorBlue() ?? .clear
        statusBtn.secondColor = R.color.textColorBlue() ?? .clear
        statusBtn.thirdColor = UIColor(red: 69/255, green: 212/255, blue: 207/255, alpha: 1)
    }
}
extension ReservationDetailsVC {
    override func backBtn(_ sender: Any) {
        router?.dismiss()
    }
    @IBAction func accept(_ sender: Any) {
        router?.acceptReservation(for: reservation)
    }
    @IBAction func reject(_ sender: Any) {
        router?.rejectReservation(for: reservation)
    }
}
// MARK: - ...  View Contract
extension ReservationDetailsVC: ReservationDetailsViewContract, PopupConfirmationViewContract {

}

extension ReservationDetailsVC: AcceptOrderDelegate {
    func acceptOrder(_ delegate: AcceptOrderDelegate?, reservation: ReservationsModel.Datum?) {
        router?.dismiss()
    }
}
extension ReservationDetailsVC: RejectOrderDelegate {
    func rejectOrder(_ delegate: RejectOrderDelegate?, reservation: ReservationsModel.Datum?) {
        router?.dismiss()
    }
}

//
//  OrderDetailsContract.swift
//  DAL_IOS
//
//  Created by M.abdu on 2/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol ReservationDetailsPresenterContract: PresenterProtocol {
    
}
// MARK: - ...  View Contract
protocol ReservationDetailsViewContract: PresentingViewProtocol {
 
}
// MARK: - ...  Router Contract
protocol ReservationDetailsRouterContract: Router, RouterProtocol {
    func dismiss()
    func acceptReservation(for reservation: ReservationsModel.Datum?)
    func rejectReservation(for reservation: ReservationsModel.Datum?)
}

protocol ReservationDetailsDelegate: class {
    func reservationDetails(_ delegate: ReservationDetailsDelegate?)
}

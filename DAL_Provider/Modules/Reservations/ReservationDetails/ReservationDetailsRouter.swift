//
//  OrderDetailsRouter.swift
//  DAL_IOS
//
//  Created by M.abdu on 2/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class ReservationDetailsRouter: Router {
    typealias PresentingView = ReservationDetailsVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension ReservationDetailsRouter: ReservationDetailsRouterContract {
    func dismiss() {
        view?.delegate?.reservationDetails(view?.delegate)
        view?.dismiss(animated: true, completion: nil)
    }
    func acceptReservation(for reservation: ReservationsModel.Datum?) {
        guard let scene = R.storyboard.acceptOrderStoryboard.acceptOrderVC() else { return }
        scene.delegate = view
        scene.type = .reservation
        scene.reservation = reservation
        view?.pushPop(scene)
    }
    func rejectReservation(for reservation: ReservationsModel.Datum?) {
        guard let scene = R.storyboard.rejectOrderStoryboard.rejectOrderVC() else { return }
        scene.delegate = view
        scene.type = .reservation
        scene.reservation = reservation
        view?.pushPop(scene)
    }
}

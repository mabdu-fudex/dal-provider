//
//  OrderCell.swift
//  DAL_IOS
//
//  Created by M.abdu on 2/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import UIKit

protocol ReservationCellDelegate: class {
    func reservationCell(_ cell: ReservationCell?, accept: Bool?)
    func reservationCell(_ cell: ReservationCell?, cancel: Bool?)
    func reservationCell(_ cell: ReservationCell?, finish: Bool?)
}
class ReservationCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var mealsLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var newOrderView: UIView!
    @IBOutlet weak var processingView: UIView!
    @IBOutlet weak var timerBtn: UIButton!
    @IBOutlet weak var finishBtn: GradientButton!
    
    
    var tab: OrdersVC.Tab = .new
    weak var delegate: ReservationCellDelegate?
    func setup() {
        guard let model = model as? ReservationsModel.Datum? else { return }
        userImage.setImage(url: model?.userImg)
        usernameLbl.text = model?.userName
        let dateDay = DateHelper().date(date: model?.reservationDate, format: "d MMM", oldFormat: "yyyy-MM-dd")
        let dateHour = DateHelper().date(date: model?.reservationTime, format: "h:mm a", oldFormat: "HH:mm:ss")
        dateLbl.text = "\("day".localized) \(dateDay ?? "") , \("hour".localized) \(dateHour ?? "")"
        mealsLbl.text = model?.featureName
        
        if tab == .new {
            newOrderView.isHidden = false
            processingView.isHidden = true
            statusLbl.text = "\("count".localized) \(model?.qty ?? 0) \("persons".localized)"
            statusLbl.textColor = R.color.thirdTextColor()
        } else if tab == .processing {
            newOrderView.isHidden = true
            processingView.isHidden = false
            statusLbl.text = "\("count".localized) \(model?.qty ?? 0) \("persons".localized)"
            statusLbl.textColor = R.color.thirdTextColor()
            timerBtn.setTitle("\("Remaining".localized) \(model?.remaining ?? "0") \("Minute".localized)", for: .normal)
        } else {
            newOrderView.isHidden = true
            processingView.isHidden = true
            statusLbl.text = model?.statusText
            if model?.status == 3 {
                statusLbl.textColor = R.color.textColorBlue()
            } else if model?.status == 4 {
                statusLbl.textColor = R.color.thirdColor()
            }
        }
    }
    @IBAction func reject(_ sender: Any) {
        delegate?.reservationCell(self, cancel: true)
    }
    @IBAction func accept(_ sender: Any) {
        delegate?.reservationCell(self, accept: true)
    }
    @IBAction func finish(_ sender: Any) {
        delegate?.reservationCell(self, finish: true)
    }
}

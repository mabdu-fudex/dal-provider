//
//  NotificationsPresenter.swift
//  DAL_IOS
//
//  Created by M.abdu on 1/20/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class NotificationsPresenter: BasePresenter<NotificationsViewContract> {
}
// MARK: - ...  Presenter Contract
extension NotificationsPresenter: NotificationsPresenterContract {
}
// MARK: - ...  Example of network response
extension NotificationsPresenter {
    func fetchResponse<T: NotificationsModel>(response: NetworkResponse<T>) {
        switch response {
            case .success(_):
                break
            case .failure(_):
                break
        }
    }
}

//
//  NotificationsVC.swift
//  DAL_IOS
//
//  Created by M.abdu on 1/20/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class NotificationsVC: BaseController {
    @IBOutlet weak var notificationsTbl: UITableView!
    var presenter: NotificationsPresenter?
    var router: NotificationsRouter?
}

// MARK: - ...  LifeCycle
extension NotificationsVC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension NotificationsVC {
    func setup() {
        notificationsTbl.delegate = self
        notificationsTbl.dataSource = self
    }
}
// MARK: - ...  View Contract
extension NotificationsVC: NotificationsViewContract {
}

extension NotificationsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: NotificationCell.self, indexPath)
        return cell
    }
}

//
//  NotificationsContract.swift
//  DAL_IOS
//
//  Created by M.abdu on 1/20/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol NotificationsPresenterContract: PresenterProtocol {
}
// MARK: - ...  View Contract
protocol NotificationsViewContract: PresentingViewProtocol {
}
// MARK: - ...  Router Contract
protocol NotificationsRouterContract: Router, RouterProtocol {
}

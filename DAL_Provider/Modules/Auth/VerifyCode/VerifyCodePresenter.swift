//
//  VerifyCodePresenter.swift
//  DAL_IOS
//
//  Created by M.abdu on 12/28/20.
//  Copyright © 2020 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class VerifyCodePresenter: BasePresenter<VerifyCodeViewContract> {

    func verifyCode(mobile: String?, code: Int?) {
        NetworkManager.instance.paramaters["phone"] = mobile
        NetworkManager.instance.paramaters["code"] = code
        NetworkManager.instance.request(.checkCode, type: .post, VerifyCodeModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didVerify(code: model?.data)
                case .failure(let error):
                    self?.view?.didError(error: error?.localizedDescription)
            }
        }
    }
    func resendCode(mobile: String?) {
        NetworkManager.instance.paramaters["phone"] = mobile
        NetworkManager.instance.request(.resendCode, type: .post, BaseModel<Int>.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didResend(code: model?.data)
                case .failure(let error):
                    self?.view?.didError(error: error?.localizedDescription)
            }
        }
    }
}
// MARK: - ...  Presenter Contract
extension VerifyCodePresenter: VerifyCodePresenterContract {
}
 

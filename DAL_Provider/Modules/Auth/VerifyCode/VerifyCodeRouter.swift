//
//  VerifyCodeRouter.swift
//  DAL_IOS
//
//  Created by M.abdu on 12/28/20.
//  Copyright © 2020 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class VerifyCodeRouter: Router {
    typealias PresentingView = VerifyCodeVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension VerifyCodeRouter: VerifyCodeRouterContract {
    func resetPassword() {
        guard let scene = R.storyboard.resetPasswordStoryboard.resetPasswordVC() else { return }
        scene.mobile = view?.mobile
        scene.countryCode = view?.countryCode
        view?.push(scene)
    }
    func completeRegister() {
        guard let scene = R.storyboard.completeRegisterStep1Storyboard.completeRegisterStep1VC() else { return }
        scene.mobile = view?.mobile
        scene.countryCode = view?.countryCode
        view?.push(scene)
//
//        guard let scene = R.storyboard.completeRegisterStep3Storyboard.completeRegisterStep3VC() else { return }
//        scene.userID = 19
//        view?.push(scene)
    }
}

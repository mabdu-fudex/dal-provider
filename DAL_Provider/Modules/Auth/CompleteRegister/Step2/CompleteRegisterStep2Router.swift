//
//  CompleteRegisterStep2Router.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class CompleteRegisterStep2Router: Router {
    typealias PresentingView = CompleteRegisterStep2VC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension CompleteRegisterStep2Router: CompleteRegisterStep2RouterContract {
    func goToStep3(withUser id: Int) {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.050) {
            guard let scene = R.storyboard.completeRegisterStep3Storyboard.completeRegisterStep3VC() else { return }
            scene.userID = id
            if self.view?.mainProvider != nil {
                scene.mainProvider = self.view?.mainProvider
            }
            self.view?.push(scene)
        }
    }
}

//
//  CompleteRegisterStep2VC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class CompleteRegisterStep2VC: BaseController {
    @IBOutlet weak var logoBtn: UIButton!
    @IBOutlet weak var resturantNameTxf: UITextField!
    @IBOutlet weak var nationalityCollection: TagCollectionView!
    @IBOutlet weak var servicesCollection: TagCollectionView!
    @IBOutlet weak var differnetYesBtn: RadioButton!
    @IBOutlet weak var differnetNoBtn: RadioButton!
    @IBOutlet weak var meatTypeLbl: UILabel!
    @IBOutlet weak var meatTypeView: UIView!
    @IBOutlet weak var meatTypeTxf: UITextField!
    @IBOutlet weak var meatSourceLbl: UILabel!
    @IBOutlet weak var meatSourceCollection: TagCollectionView!
    @IBOutlet weak var externalMeatSourceLbl: UILabel!
    @IBOutlet weak var externalMeatSourceView: UIView!
    @IBOutlet weak var externalMeatSourceTxf: UITextField!
    
    var presenter: CompleteRegisterStep2Presenter?
    var router: CompleteRegisterStep2Router?
    var userID: Int?
    var mainProvider: UserRoot.User?
    var features: [ServiceTypeModel.Datum] = []
    var nationality: [ServiceTypeModel.Datum] = []
    var isDiff: Bool?
    var avatarImage: UIImage?
    lazy var imagePicker: GalleryPickerHelper? = {
        let picker = GalleryPickerHelper()
        picker.onPickImage = { [weak self] image in
            self?.logoBtn.setImage(image, for: .normal)
            self?.avatarImage = image
        }
        return picker
    }()
}

// MARK: - ...  LifeCycle
extension CompleteRegisterStep2VC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
        fetch()
        if mainProvider != nil {
            setupMainProvider()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}
// MARK: - ...  Functions
extension CompleteRegisterStep2VC {
    func fetch() {
        startLoading()
        presenter?.fetchNationality()
    }
    func reload() {
        nationalityCollection.delegate = self
        nationalityCollection.dataSource = self
        servicesCollection.delegate = self
        servicesCollection.dataSource = self
        meatSourceCollection.delegate = self
        meatSourceCollection.dataSource = self
        meatSourceCollection.onlyItemSelected = true
    }
    func setup() {
        setupRadioBtns()
    }
    func setupRadioBtns() {
        differnetYesBtn.onSelect { [weak self] in
            self?.isDiff = true
            self?.differnetNoBtn.deselect()
        }
        differnetYesBtn.onDeselect { [weak self] in
            if self?.isDiff == true {
                self?.isDiff = nil
            }
        }
        differnetNoBtn.onSelect { [weak self] in
            self?.isDiff = false
            self?.differnetYesBtn.deselect()
        }
        differnetNoBtn.onDeselect { [weak self] in
            if self?.isDiff == false {
                self?.isDiff = nil
            }
        }
    }
    func setupMainProvider() {
        if mainProvider?.attributes?.mix == 1 {
            differnetYesBtn.select()
        } else {
            differnetNoBtn.select()
        }
        
        resturantNameTxf.text = mainProvider?.name
        meatTypeTxf.text = mainProvider?.attributes?.meatType
        externalMeatSourceTxf.text = mainProvider?.attributes?.otherMeatSource
         
        
        var arrayNationality: [ServiceTypeModel.Datum] = []
        var pathesNationality: [Int] = []
        let model = ServiceTypeModel.Datum(id: mainProvider?.attributes?.nationalityID, img: nil, name: mainProvider?.attributes?.nationality?.name, active: 1)
        arrayNationality.append(model)
        for index in 0..<nationality.count {
            if nationality[index].id == model.id {
                pathesNationality.append(index)
            }
        }
        nationalityCollection.selectedItemsTags = arrayNationality
        nationalityCollection.selectedItemsPaths = pathesNationality
        nationalityCollection.reload()
        
        if mainProvider?.attributes?.meatSource == "EXTERNAL" {
            meatSourceCollection.selectedItemsTags = [ServiceTypeModel.Datum(id: 0, img: nil, name: "EXTERNAL", active: 0)]
            meatSourceCollection.selectedItemsPaths = [0]
            meatSourceCollection.reload()
        } else {
            meatSourceCollection.selectedItemsTags = [ServiceTypeModel.Datum(id: 1, img: nil, name: "INTERNAL", active: 0)]
            meatSourceCollection.selectedItemsPaths = [1]
            meatSourceCollection.reload()
        }
         
        var arrayServices: [ServiceTypeModel.Datum] = []
        var pathesServices: [Int] = []
        for item in mainProvider?.features ?? [] {
            let model = ServiceTypeModel.Datum(id: item.featureID, img: nil, name: item.feature?.name, active: 1)
            arrayServices.append(model)
            for index in 0..<features.count {
                if features[index].id == model.id {
                    pathesServices.append(index)
                }
            }
        }
        servicesCollection.selectedItemsTags = arrayServices
        servicesCollection.selectedItemsPaths = pathesServices
        servicesCollection.reload()
        
    }
}
extension CompleteRegisterStep2VC {
    @IBAction func serviceLogo(_ sender: Any) {
        imagePicker?.pick(in: self)
    }
    @IBAction func next(_ sender: Any) {
        if isDiff == nil {
            openConfirmation(title: "Please choose whether the restaurant is diversified or not".localized)
            return
        }
        presenter?.registerStep2()
    }
}
// MARK: - ...  View Contract
extension CompleteRegisterStep2VC: CompleteRegisterStep2ViewContract, PopupConfirmationViewContract {
    func didFetchNationality(list: [ServiceTypeModel.Datum]?) {
        presenter?.fetchFeatures()
        nationality.append(contentsOf: list ?? [])
    }
    func didFetchFeatures(list: [ServiceTypeModel.Datum]?) {
        stopLoading()
        features.append(contentsOf: list ?? [])
        reload()
    }
    func didSuccessStep2() {
        router?.goToStep3(withUser: userID ?? 0)
    }
    func didFailStep2(error: String?) {
        openConfirmation(title: error)
    }
}

extension CompleteRegisterStep2VC: TagCollectionViewDataSource, TagCollectionViewDelegate {
    func tagCollectionView(for collection: TagCollectionView?) -> [TagModel] {
        if collection == servicesCollection {
            return features
        } else if collection == nationalityCollection {
            return nationality
        } else {
            let model = ServiceTypeModel.Datum(id: 0, img: nil, name: "EXTERNAL".localized, active: 1)
            let model1 = ServiceTypeModel.Datum(id: 1, img: nil, name: "INTERNAL".localized, active: 1)
            return [model, model1]
        }
    }
}

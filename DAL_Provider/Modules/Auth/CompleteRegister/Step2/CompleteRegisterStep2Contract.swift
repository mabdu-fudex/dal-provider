//
//  CompleteRegisterStep2Contract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol CompleteRegisterStep2PresenterContract: PresenterProtocol {
    func fetchFeatures()
    func fetchNationality()
    func registerStep2()
}
// MARK: - ...  View Contract
protocol CompleteRegisterStep2ViewContract: PresentingViewProtocol {
    func didFetchFeatures(list: [ServiceTypeModel.Datum]?)
    func didFetchNationality(list: [ServiceTypeModel.Datum]?)
    func didSuccessStep2()
    func didFailStep2(error: String?)
}
// MARK: - ...  Router Contract
protocol CompleteRegisterStep2RouterContract: Router, RouterProtocol {
    func goToStep3(withUser id: Int)
}

//
//  CompleteRegisterStep2Presenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class CompleteRegisterStep2Presenter: BasePresenter<CompleteRegisterStep2ViewContract> {
}
// MARK: - ...  Presenter Contract
extension CompleteRegisterStep2Presenter: CompleteRegisterStep2PresenterContract {
}
// MARK: - ...  Example of network response
extension CompleteRegisterStep2Presenter {
    func fetchFeatures() {
        NetworkManager.instance.request(.features, type: .get, ServiceTypeModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFetchFeatures(list: model?.data)
                case .failure:
                    break
            }
        }
    }
    func fetchNationality() {
        NetworkManager.instance.request(.nationalites, type: .get, ServiceTypeModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFetchNationality(list: model?.data)
                case .failure:
                    break
            }
        }
    }
    func registerStep2() {
        guard let view = view as? CompleteRegisterStep2VC else { return }
        var paramters: [String: Any] = [:]
        paramters["user_id"] = view.userID
        paramters["provider_name"] = view.resturantNameTxf.text
        paramters["nationality_id"] = view.nationalityCollection.selectedItemsTags.first?.id
        paramters["meat_type"] = view.meatTypeTxf.text
        paramters["meat_source"] = view.meatSourceCollection.selectedItemsTags.first?.name
        paramters["other_meat_source"] = view.externalMeatSourceTxf.text
        
        var ids: [Int] = []
        for index in 0..<view.servicesCollection.selectedItemsTags.count {
            ids.append(view.servicesCollection.selectedItemsTags[index].id ?? 0)
        }
        paramters["features"] = ids.toJson()

        if case view.isDiff = true {
            paramters["mix"] = "1"
        } else {
            paramters["mix"] = "0"
        }
        view.startLoading()
        NetworkManager.instance.paramaters = paramters
        let method = NetworkConfigration.EndPoint.endPoint(point: .providerRegisterStep2, paramters: [])
        NetworkManager.instance.uploadMultiImages(method, type: .post, file: ["avatar": view.avatarImage], CompleteRegisterStep1Model.self) { [weak self] (response) in
            self?.view?.stopLoading()
            switch response {
                case .success:
                    self?.view?.didSuccessStep2()
                case .failure(let error):
                    self?.view?.didFailStep2(error: error?.localizedDescription)
            }
        }
    }
}

//
//  UploadDocumentRouter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class UploadDocumentRouter: Router {
    typealias PresentingView = UploadDocumentVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension UploadDocumentRouter: UploadDocumentRouterContract {
    func goToHome() {
        restart()
    }
}

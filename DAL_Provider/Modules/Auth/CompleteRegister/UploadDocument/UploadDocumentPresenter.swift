//
//  UploadDocumentPresenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class UploadDocumentPresenter: BasePresenter<UploadDocumentViewContract> {
}
// MARK: - ...  Presenter Contract
extension UploadDocumentPresenter: UploadDocumentPresenterContract {
    func uploadReceipt() {
        guard let view = view as? UploadDocumentVC else { return }
        var paramters: [String: Any] = [:]
        paramters["user_id"] = UserRoot.fetchUser()?.id
        view.startLoading()
        NetworkManager.instance.paramaters = paramters
        let method = NetworkConfigration.EndPoint.endPoint(point: .uploadReceipt, paramters: [])
        NetworkManager.instance.uploadMultiImages(method, type: .post, file: ["receipt_img": view.uploadedImage], UserRoot.self) { [weak self] (response) in
            self?.view?.stopLoading()
            switch response {
                case .success(let model):
                    model?.save()
                    self?.view?.didUploadReceipt()
                case .failure(let error):
                    self?.view?.didFailUpload(error: error?.localizedDescription)
            }
        }
    }
}
 

//
//  UploadDocumentContract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol UploadDocumentPresenterContract: PresenterProtocol {
    func uploadReceipt()
}
// MARK: - ...  View Contract
protocol UploadDocumentViewContract: PresentingViewProtocol {
    func didUploadReceipt()
    func didFailUpload(error: String?)
}
// MARK: - ...  Router Contract
protocol UploadDocumentRouterContract: Router, RouterProtocol {
    func goToHome()
}

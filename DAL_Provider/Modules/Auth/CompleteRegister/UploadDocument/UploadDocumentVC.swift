//
//  UploadDocumentVC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class UploadDocumentVC: BaseController {
    @IBOutlet weak var cameraImage: UIImageView!
    @IBOutlet weak var galleryImage: UIImageView!
    var presenter: UploadDocumentPresenter?
    var router: UploadDocumentRouter?
    var fromGallery: Bool = true
    var uploadedImage: UIImage?
    lazy var imagePicker: GalleryPickerHelper? = {
        let picker = GalleryPickerHelper()
        picker.onPickImage = { [weak self] image in
            if case self?.fromGallery = true {
                self?.galleryImage.image = image
                self?.cameraImage.image = nil
            } else {
                self?.cameraImage.image = image
                self?.galleryImage.image = nil
            }
            self?.uploadedImage = image
        }
        return picker
    }()
}

// MARK: - ...  LifeCycle
extension UploadDocumentVC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension UploadDocumentVC {
    func setup() {
    }
}
extension UploadDocumentVC {
    @IBAction func uploadGallery(_ sender: Any) {
        fromGallery = true
        imagePicker?.pickGallery(in: self)
    }
    @IBAction func uploadCamera(_ sender: Any) {
        fromGallery = false
        imagePicker?.pickCamera(in: self)
    }
    @IBAction func send(_ sender: Any) {
        presenter?.uploadReceipt()
    }
}
// MARK: - ...  View Contract
extension UploadDocumentVC: UploadDocumentViewContract, PopupConfirmationViewContract {
    func didUploadReceipt() {
        router?.goToHome()
    }
    func didFailUpload(error: String?) {
        openConfirmation(title: error)
    }
}

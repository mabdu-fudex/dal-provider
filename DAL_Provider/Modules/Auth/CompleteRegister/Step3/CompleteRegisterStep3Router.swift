//
//  CompleteRegisterStep3Router.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class CompleteRegisterStep3Router: Router {
    typealias PresentingView = CompleteRegisterStep3VC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension CompleteRegisterStep3Router: CompleteRegisterStep3RouterContract {
    func goToMembership() {
        guard let scene = R.storyboard.membershipStoryboard.membershipVC() else { return }
        self.view?.push(scene)
    }
}

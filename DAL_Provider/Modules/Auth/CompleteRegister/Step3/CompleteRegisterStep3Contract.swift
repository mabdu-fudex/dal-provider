//
//  CompleteRegisterStep3Contract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol CompleteRegisterStep3PresenterContract: PresenterProtocol {
    func registerStep3()
}
// MARK: - ...  View Contract
protocol CompleteRegisterStep3ViewContract: PresentingViewProtocol {
    func didSuccessStep3()
    func didFailStep3(error: String?)
}
// MARK: - ...  Router Contract
protocol CompleteRegisterStep3RouterContract: Router, RouterProtocol {
    func goToMembership()
}

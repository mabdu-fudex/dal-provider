//
//  CompleteRegisterStep3VC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class CompleteRegisterStep3VC: BaseController {
    @IBOutlet weak var freeDeliveryTxf: UITextField!
    @IBOutlet weak var minimumDeliveryTxf: UITextField!
    @IBOutlet weak var deliveryYesBtn: RadioButton!
    @IBOutlet weak var deliveryNoBtn: RadioButton!
    @IBOutlet weak var tableYesBtn: RadioButton!
    @IBOutlet weak var tableNoBtn: RadioButton!
    @IBOutlet weak var taxYesBtn: RadioButton!
    @IBOutlet weak var taxNoBtn: RadioButton!
    @IBOutlet weak var workingHourBtn: UIButton!
    @IBOutlet weak var fromTimeBtn: UIButton!
    @IBOutlet weak var toTimeBtn: UIButton!
    @IBOutlet weak var aboutPlaceTxf: UITextField!
    @IBOutlet weak var imagesView: UIView!
    @IBOutlet weak var imagesCollection: UICollectionView!
    
    var presenter: CompleteRegisterStep3Presenter?
    var router: CompleteRegisterStep3Router?
    var userID: Int?
    var mainProvider: UserRoot.User?
    var isDelivery: Bool?
    var isReserveTable: Bool?
    var isTax: Bool?
    var workingHours: [String] = ["1", "2", "3", "4","5", "6", "7", "8","9", "10", "11", "12","13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"]
    var fromHour: String?
    var toHour: String?
    var workHours: String?
    var images: [UIImage] = []
    var editedImagePath: Int?
    lazy var imagePicker: GalleryPickerHelper? = {
        let picker = GalleryPickerHelper()
        picker.onPickImage = { [weak self] image in
            if self?.editedImagePath != nil {
                self?.images[self?.editedImagePath ?? 0] = image
                self?.editedImagePath = nil
            } else {
                self?.images.append(image)
            }
            self?.imagesView.isHidden = false
            self?.imagesCollection.reloadData()
        }
        return picker
    }()
    
}

// MARK: - ...  LifeCycle
extension CompleteRegisterStep3VC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
        if mainProvider != nil {
            setupMainProvider()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}
// MARK: - ...  Functions
extension CompleteRegisterStep3VC {
    func setup() {
        setupRadioBtns()
        imagesCollection.delegate = self
        imagesCollection.dataSource = self
    }
    func setupRadioBtns() {
        setupDeliveryRadioBtns()
        setupTableRadioBtns()
        setupTaxRadioBtns()
    }
    func setupDeliveryRadioBtns() {
        deliveryYesBtn.onSelect { [weak self] in
            self?.isDelivery = true
            self?.deliveryNoBtn.deselect()
        }
        deliveryYesBtn.onDeselect { [weak self] in
            if self?.isDelivery == true {
                self?.isDelivery = nil
            }
        }
        deliveryNoBtn.onSelect { [weak self] in
            self?.isDelivery = false
            self?.deliveryYesBtn.deselect()
        }
        deliveryNoBtn.onDeselect { [weak self] in
            if self?.isDelivery == false {
                self?.isDelivery = nil
            }
        }
        
    }
    func setupTableRadioBtns() {
        tableYesBtn.onSelect { [weak self] in
            self?.isReserveTable = true
            self?.tableNoBtn.deselect()
        }
        tableYesBtn.onDeselect { [weak self] in
            if self?.isReserveTable == true {
                self?.isReserveTable = nil
            }
        }
        tableNoBtn.onSelect { [weak self] in
            self?.isReserveTable = false
            self?.tableYesBtn.deselect()
        }
        tableNoBtn.onDeselect { [weak self] in
            if self?.isReserveTable == false {
                self?.isReserveTable = nil
            }
        }
    }
    func setupTaxRadioBtns() {
        taxYesBtn.onSelect { [weak self] in
            self?.isTax = true
            self?.taxNoBtn.deselect()
        }
        taxYesBtn.onDeselect { [weak self] in
            if self?.isTax == true {
                self?.isTax = nil
            }
        }
        taxNoBtn.onSelect { [weak self] in
            self?.isTax = false
            self?.taxYesBtn.deselect()
        }
        taxNoBtn.onDeselect { [weak self] in
            if self?.isTax == false {
                self?.isTax = nil
            }
        }
        
    }
    func setupMainProvider() {
        if mainProvider?.attributes?.deliveryOption == 1 {
            deliveryYesBtn.select()
        } else {
            deliveryNoBtn.select()
        }
        if mainProvider?.attributes?.tableOption == 1 {
            tableYesBtn.select()
        } else {
            tableNoBtn.select()
        }
        if mainProvider?.attributes?.priceAdded == 1 {
            taxYesBtn.select()
        } else {
            taxNoBtn.select()
        }
        
        minimumDeliveryTxf.text = mainProvider?.attributes?.minimumOrderPrice
        freeDeliveryTxf.text = mainProvider?.attributes?.freeOrderPrice
        workHours = mainProvider?.attributes?.workHours
        toHour = mainProvider?.attributes?.to
        fromHour = mainProvider?.attributes?.from
        workingHourBtn.setTitle(workHours, for: .normal)
        fromTimeBtn.setTitle(workHours, for: .normal)
        toTimeBtn.setTitle(workHours, for: .normal)
        aboutPlaceTxf.text = mainProvider?.attributes?.detailsAr
    }
}

extension CompleteRegisterStep3VC {
    @IBAction func workingHour(_ sender: Any) {
        guard let scene = R.storyboard.pickerViewHelper.pickerController() else { return }
        scene.source = workingHours
        scene.titleClosure = { [weak self] row in
            return self?.workingHours[row]
        }
        scene.didSelectClosure = { [weak self] row in
            let item = self?.workingHours[row]
            self?.workingHourBtn.setTitle(item, for: .normal)
            self?.workHours = item
        }
        pushPop(scene)
    }
    @IBAction func fromTime(_ sender: Any) {
        guard let scene = R.storyboard.pickTimeController.pickTimeController() else { return }
        scene.type = .time
        scene.closureTime = { [weak self] date in
            let time = DateHelper().date(date: date, format: "h:mm a")
            self?.fromTimeBtn.setTitle(time, for: .normal)
            DateHelper.currentLocal = .english
            self?.fromHour = DateHelper().date(date: date, format: "HH:mm")
            DateHelper.currentLocal = nil
        }
        pushPop(scene)
    }
    @IBAction func toTime(_ sender: Any) {
        guard let scene = R.storyboard.pickTimeController.pickTimeController() else { return }
        scene.type = .time
        scene.closureTime = { [weak self] date in
            let time = DateHelper().date(date: date, format: "h:mm a")
            self?.toTimeBtn.setTitle(time, for: .normal)
            DateHelper.currentLocal = .english
            self?.toHour = DateHelper().date(date: date, format: "HH:mm")
            DateHelper.currentLocal = nil
        }
        pushPop(scene)
    }
    @IBAction func uploadPhotos(_ sender: Any) {
        if images.count >= 8 {
            return
        }
        imagePicker?.pick(in: self)
    }
    @IBAction func next(_ sender: Any) {
        presenter?.registerStep3()
    }
}
// MARK: - ...  View Contract
extension CompleteRegisterStep3VC: CompleteRegisterStep3ViewContract, PopupConfirmationViewContract {
    func didSuccessStep3() {
        router?.goToMembership()
    }
    func didFailStep3(error: String?) {
        openConfirmation(title: error)
    }
}


// MARK: - ...  Collection view
extension CompleteRegisterStep3VC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 60)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = imagesCollection.cell(type: RequsetImageCell.self, indexPath)
        cell.delegate = self
        cell.canPlus = false
        cell.image = images[indexPath.row]
        cell.setup()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? RequsetImageCell
        if case cell?.canPlus = false {
            editImage(indexPath.row)
        }
    }
    func editImage(_ path: Int) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.view.tintColor = .darkGray
        let acceptAction = UIAlertAction(title: "Edit".localized, style: .default) { [weak self] (_) -> Void in
            self?.editedImagePath = path
            self?.didPlusImage()
        }
        alert.addAction(acceptAction)
        let deleteAction = UIAlertAction(title: "Delete".localized, style: .default) { [weak self] (_) -> Void in
            self?.images.remove(at: path)
            self?.imagesCollection.reloadData()
        }
        alert.addAction(deleteAction)
        
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}
// MARK: - ...  Image cell
extension CompleteRegisterStep3VC: RequestImageCellDelegate {
    func didPlusImage() {
        imagePicker?.pick(in: self)
    }
}

//
//  RequsetImageCell.swift
//  Mutsawiq
//
//  Created by M.abdu on 11/16/20.
//  Copyright © 2020 com.Rowaad. All rights reserved.
//

import UIKit

protocol RequestImageCellDelegate: class {
    func didPlusImage()
}
class RequsetImageCell: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var addMoreView: GradientView!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var imageCell: UIImageView!
    
    var canPlus: Bool = false
    var image: UIImage?
    weak var delegate: RequestImageCellDelegate?
    func setup() {
        if canPlus {
            addMoreView.isHidden = false
            imageView.isHidden = true
        } else {
            addMoreView.isHidden = true
            imageView.isHidden = false
            imageCell.image = image
        }
    }
    @IBAction func plus(_ sender: Any) {
        delegate?.didPlusImage()
    }
}

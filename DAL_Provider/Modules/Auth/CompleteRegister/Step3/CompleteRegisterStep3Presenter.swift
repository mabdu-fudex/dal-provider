//
//  CompleteRegisterStep3Presenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class CompleteRegisterStep3Presenter: BasePresenter<CompleteRegisterStep3ViewContract> {
}
// MARK: - ...  Presenter Contract
extension CompleteRegisterStep3Presenter: CompleteRegisterStep3PresenterContract {
    func registerStep3() {
        guard let view = view as? CompleteRegisterStep3VC else { return }
        var paramters: [String: Any] = [:]
        paramters["user_id"] = view.userID
        paramters["minimum_order_price"] = view.minimumDeliveryTxf.text
        paramters["free_order_price"] = view.freeDeliveryTxf.text
        paramters["work_hours"] = view.workHours
        paramters["from"] = view.fromHour
        paramters["to"] = view.toHour
        paramters["details_ar"] = view.aboutPlaceTxf.text
        paramters["details_en"] = view.aboutPlaceTxf.text
        if case view.isDelivery = true {
            paramters["delivery_option"] = "1"
        } else {
            paramters["delivery_option"] = "0"
        }
        if case view.isReserveTable = true {
            paramters["table_option"] = "1"
        } else {
            paramters["table_option"] = "0"
        }
        if case view.isTax = true {
            paramters["price_added"] = "1"
        } else {
            paramters["price_added"] = "0"
        }
        view.startLoading()
        NetworkManager.instance.paramaters = paramters
        let method = NetworkConfigration.EndPoint.endPoint(point: .providerRegisterStep3, paramters: [])
        NetworkManager.instance.uploadMultiImages(method, type: .post, files: view.images, key: "providerimages", UserRoot.self) { [weak self] (response) in
            self?.view?.stopLoading()
            switch response {
                case .success(let model):
                    model?.save()
                    self?.view?.didSuccessStep3()
                case .failure(let error):
                    self?.view?.didFailStep3(error: error?.localizedDescription)
            }
        }
    }
}
// MARK: - ...  Example of network response
extension CompleteRegisterStep3Presenter {
    
}

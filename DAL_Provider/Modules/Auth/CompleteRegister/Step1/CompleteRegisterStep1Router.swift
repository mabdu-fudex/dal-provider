//
//  CompleteRegisterStep1Router.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class CompleteRegisterStep1Router: Router {
    typealias PresentingView = CompleteRegisterStep1VC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension CompleteRegisterStep1Router: CompleteRegisterStep1RouterContract {
    func openMap() {
        guard let scene = R.storyboard.locationFromMapStoryboard.locationFromMapVC() else { return }
        scene.delegate = view
        self.view?.pushPop(scene)
    }
    func goToStep2(withUser id: Int) {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.050) {
            guard let scene = R.storyboard.completeRegisterStep2Storyboard.completeRegisterStep2VC() else { return }
            scene.userID = id
            if self.view?.mainProvider != nil {
                scene.mainProvider = self.view?.mainProvider
            }
            self.view?.push(scene)
        }
    }
}

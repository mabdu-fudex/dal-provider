//
//  CompleteRegisterStep1Model.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Entity
class CompleteRegisterStep1Model: Codable {
    let status: Bool?
    var data: Data?
    
    class Data: Codable {
        let id: Int?
    }
}

// MARK: - ServiceTypeModel
struct ServiceTypeModel: Codable {
    let status: Bool?
    let data: [Datum]?
    // MARK: - Datum
    struct Datum: Codable, TagModel {
        var id: Int?
        let img: String?
        var name: String?
        let active: Int?
    }

}
// MARK: - AllProvidersModel
struct AllProvidersModel: Codable {
    let status: Bool?
    let data: [Datum]?
    
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let name, email, phone, userType: String?
        let address, lat, lng: String?
        let avatar: String?
        let active: Int?
        let lang, createdAt, updatedAt: String?
        
        enum CodingKeys: String, CodingKey {
            case id, name, email, phone
            case userType = "user_type"
            case address, lat, lng, avatar, active, lang
            case createdAt = "created_at"
            case updatedAt = "updated_at"
        }
    }

}

//
//  CompleteRegisterStep1Contract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol CompleteRegisterStep1PresenterContract: PresenterProtocol {
    func fetchServiceTypes()
    func registerStep1()
    func fetchProvider(forProvider id: Int)
    func fetchAllProviders()
}
// MARK: - ...  View Contract
protocol CompleteRegisterStep1ViewContract: PresentingViewProtocol {
    func didFetchServiceTypes(types: [ServiceTypeModel.Datum])
    func didFetchProvider(for provider: UserRoot.User?)
    func didFetchAllProviders(for list: [AllProvidersModel.Datum]?)
    func didSuccessStep1(withUser id: Int)
    func didFailStep1(error: String?)
}
// MARK: - ...  Router Contract
protocol CompleteRegisterStep1RouterContract: Router, RouterProtocol {
    func openMap()
    func goToStep2(withUser id: Int)
}

//
//  CompleteRegisterStep1Presenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class CompleteRegisterStep1Presenter: BasePresenter<CompleteRegisterStep1ViewContract> {
}
// MARK: - ...  Presenter Contract
extension CompleteRegisterStep1Presenter: CompleteRegisterStep1PresenterContract {
    func fetchServiceTypes() {
        NetworkManager.instance.request(.categories, type: .get, ServiceTypeModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFetchServiceTypes(types: model?.data ?? [])
                case .failure:
                    break
            }
        }
    }
    func fetchAllProviders() {
        NetworkManager.instance.request(.allProviders, type: .get, AllProvidersModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFetchAllProviders(for: model?.data)
                case .failure:
                    break
            }
        } 
    }
    func fetchProvider(forProvider id: Int) {
        NetworkManager.instance.paramaters["user_id"] = id
        NetworkManager.instance.request(.profile, type: .get, UserRoot.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFetchProvider(for: model?.data)
                case .failure:
                    break
            }
        }
    }
    func registerStep1() {
        guard let view = view as? CompleteRegisterStep1VC else { return }
        if !view.validate(txfs: [view.providerNameTxf, view.providerEmailTxf, view.passwordTxf, view.confirmationPasswordTxf, view.commercialNoTxf, view.IBANTxf]) {
            return
        }
        var paramters = view.paramters
        paramters["name"] = view.providerNameTxf.text
        paramters["email"] = view.providerEmailTxf.text
        paramters["phone"] = view.mobile
        paramters["password"] = view.passwordTxf.text
        paramters["password_confirmation"] = view.confirmationPasswordTxf.text
        paramters["socail_account1"] = view.social1Txf.text
        paramters["socail_account2"] = view.social2Txf.text
        paramters["bank_name"] = view.bankNameTxf.text
        paramters["iban"] = view.IBANTxf.text
        paramters["commercial_register"] = view.commercialNoTxf.text
        paramters["accepted"] = 1
         
        var ids: [Int] = []
        for index in 0..<view.classificationCollection.selectedItemsTags.count {
            ids.append(view.classificationCollection.selectedItemsTags[index].id ?? 0)
        }
        paramters["categories"] = ids.toJson()
        
        if case view.isMain = true {
            paramters["type"] = "main"
        } else {
            paramters["type"] = "sub"
            paramters["provider_id"] = view.mainProvider?.id
        }
        view.startLoading()
        NetworkManager.instance.paramaters = paramters
        let method = NetworkConfigration.EndPoint.endPoint(point: .providerRegisterStep1, paramters: [])
        NetworkManager.instance.uploadMultiImages(method, type: .post, file: ["commercial_img": view.commercialRegisteration], CompleteRegisterStep1Model.self) { [weak self] (response) in
            self?.view?.stopLoading()
            switch response {
                case .success(let model):
                    self?.view?.didSuccessStep1(withUser: model?.data?.id ?? 0)
                case .failure(let error):
                    self?.view?.didFailStep1(error: error?.localizedDescription)
            }
        }
    }
}


//
//  CompleteRegisterStep1VC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class CompleteRegisterStep1VC: BaseController {
    @IBOutlet weak var mainResturantBtn: RadioButton!
    @IBOutlet weak var subResturantBtn: RadioButton!
    @IBOutlet weak var selectResturantsView: UIView!
    @IBOutlet weak var resturantMainBtn: UIButton!
    @IBOutlet weak var serviceTypeBtn: UIButton!
    @IBOutlet weak var providerNameTxf: UITextField!
    @IBOutlet weak var providerEmailTxf: UITextField!
    @IBOutlet weak var addressTxf: UITextField!
    @IBOutlet weak var passwordTxf: UITextField!
    @IBOutlet weak var confirmationPasswordTxf: UITextField!
    @IBOutlet weak var social1Txf: UITextField!
    @IBOutlet weak var social2Txf: UITextField!
    @IBOutlet weak var bankNameTxf: UITextField!
    @IBOutlet weak var IBANTxf: UITextField!
    @IBOutlet weak var commercialNoTxf: UITextField!
    @IBOutlet weak var commercialImage: UIImageView!
    @IBOutlet weak var uploadCommercialImage: UIImageView!
    @IBOutlet weak var agreeBtn: RadioButton!
    @IBOutlet weak var classificationCollection: TagCollectionView!
    
    var presenter: CompleteRegisterStep1Presenter?
    var router: CompleteRegisterStep1Router?
    var mobile: String?
    var countryCode: String?
    var allProviders: [AllProvidersModel.Datum] = []
    var mainProvider: UserRoot.User?
    var serviceTypes: [ServiceTypeModel.Datum] = []
    var selectedServiceType: Int?
    var isMain: Bool?
    var isAgree: Bool = false
    var paramters: [String: Any] = [:]
    var commercialRegisteration: UIImage?
    lazy var imagePicker: GalleryPickerHelper? = {
        let picker = GalleryPickerHelper()
        picker.onPickImage = { [weak self] image in
            self?.uploadCommercialImage.isHidden = true
            self?.commercialImage.image = image
            self?.commercialRegisteration = image
        }
        return picker
    }()
}


// MARK: - ...  LifeCycle
extension CompleteRegisterStep1VC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
        setup()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        presenter?.fetchServiceTypes()
        presenter?.fetchAllProviders()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}
// MARK: - ...  Functions
extension CompleteRegisterStep1VC {
    func setup() {
        selectResturantsView.isHidden = true
        setupRadioBtns()
        passwordTxf.textContentType = .oneTimeCode
        confirmationPasswordTxf.textContentType = .oneTimeCode
    }
    func setupRadioBtns() {
        mainResturantBtn.onSelect { [weak self] in
            self?.setupMainResturant()
        }
        subResturantBtn.onSelect { [weak self] in
            self?.setupSubResturant()
        }
        mainResturantBtn.onDeselect { [weak self] in
            if self?.isMain == true {
                self?.isMain = nil
            }
        }
        subResturantBtn.onDeselect { [weak self] in
            if self?.isMain == false {
                self?.isMain = nil
            }
        }
        agreeBtn.onSelect { [weak self] in
            self?.isAgree = true
        }
        agreeBtn.onDeselect { [weak self] in
            self?.isAgree = false
        }
    }
    func setupSubResturant() {
        self.isMain = false
        self.selectResturantsView.isHidden = false
        self.mainResturantBtn.deselect()
    }
    func setupMainResturant() {
        self.isMain = true
        self.selectResturantsView.isHidden = true
        self.subResturantBtn.deselect()
        self.mainProvider = nil
    }
    func setupMainProvider() {
        providerNameTxf.text = mainProvider?.attributes?.providerName
        providerEmailTxf.text = mainProvider?.email
        addressTxf.text = mainProvider?.address
        social1Txf.text = mainProvider?.attributes?.socailAccount1
        social2Txf.text = mainProvider?.attributes?.socailAccount2
        bankNameTxf.text = mainProvider?.attributes?.bankName
        IBANTxf.text = mainProvider?.attributes?.iban
        commercialNoTxf.text = mainProvider?.attributes?.commercialRegister
                
        var array: [ServiceTypeModel.Datum] = []
        var pathes: [Int] = []
        for item in mainProvider?.categories ?? [] {
            let model = ServiceTypeModel.Datum(id: item.categoryID, img: nil, name: item.category?.name, active: 1)
            array.append(model)
            for index in 0..<serviceTypes.count {
                if serviceTypes[index].id == model.id {
                    pathes.append(index)
                }
            }
        }
        classificationCollection.selectedItemsTags = array
        classificationCollection.selectedItemsPaths = pathes
        classificationCollection.reload()
        
        
        paramters["lat"] = mainProvider?.lat
        paramters["lng"] = mainProvider?.lng
        paramters["address"] = mainProvider?.address
    }
}
// MARK: - ...  Actions
extension CompleteRegisterStep1VC {
    @IBAction func selectResturants(_ sender: Any) {
        guard let scene = R.storyboard.pickerViewHelper.pickerController() else { return }
        scene.source = allProviders
        scene.titleClosure = { [unowned self] row in
            return self.allProviders[row].name
        }
        scene.didSelectClosure = { [unowned self] row in
            let item = self.allProviders[row]
            self.resturantMainBtn.setTitle(item.name, for: .normal)
            self.startLoading()
            self.presenter?.fetchProvider(forProvider: item.id ?? 0)
        }
        pushPop(scene)
    }
    @IBAction func serviceType(_ sender: Any) {

    }
    @IBAction func gps(_ sender: Any) {
        self.router?.openMap()
    }
    @IBAction func commercialUpload(_ sender: Any) {
        self.imagePicker?.pick(in: self)
    }
    @IBAction func agreeTerms(_ sender: Any) {
        isAgree ? agreeBtn.deselect() : agreeBtn.select()
    }
    @IBAction func next(_ sender: Any) {
        if isMain == nil {
            openConfirmation(title: "Please select the type of resturant".localized)
            return
        }
        if !isAgree {
            openConfirmation(title: "Please agree the terms first".localized)
            return
        }
        self.presenter?.registerStep1()
    }
}
// MARK: - ...  View Contract
extension CompleteRegisterStep1VC: CompleteRegisterStep1ViewContract, PopupConfirmationViewContract {
    func didFetchAllProviders(for list: [AllProvidersModel.Datum]?) {
        self.allProviders.removeAll()
        self.allProviders.append(contentsOf: list ?? [])
    }
    func didFetchProvider(for provider: UserRoot.User?) {
        self.stopLoading()
        self.mainProvider = provider
        self.setupMainProvider()
    }
    func didSuccessStep1(withUser id: Int) {
        self.router?.goToStep2(withUser: id)
    }
    func didFailStep1(error: String?) {
        self.openConfirmation(title: error)
    }
    func didFetchServiceTypes(types: [ServiceTypeModel.Datum]) {
        serviceTypes.append(contentsOf: types)
        classificationCollection.delegate = self
        classificationCollection.dataSource = self
    }
    
}

extension CompleteRegisterStep1VC: LocationFromMapDelegateContract {
    func saveLocation(lat: Double?, lng: Double?, address: String?) {
        paramters["lat"] = lat
        paramters["lng"] = lng
        paramters["address"] = address
        addressTxf.text = address
    }
    func didFailLocation() {
        
    }
}

extension CompleteRegisterStep1VC: TagCollectionViewDelegate, TagCollectionViewDataSource {
    func tagCollectionView(for collection: TagCollectionView?) -> [TagModel] {
        return serviceTypes
    }
}
//extension CompleteRegisterStep1VC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let width = 40 + (serviceTypes[indexPath.row].name?.widthWithConstrainedWidth(width: collectionView.width, font: ThemeApp.Fonts.regularFont(size: 16)) ?? 40)
//        return CGSize(width: width, height: 40)
//    }
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return serviceTypes.count
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.cell(type: TagCollectionCell.self, indexPath)
//        cell.tagNameLbl.text = serviceTypes[indexPath.row].name
//        return cell
//    }
//}

//extension {
//    if let height = textView.constraints.first(where: { $0.firstAttribute == .height }) {
//        if textViewsHeight[self] == nil {
//            textViewsHeight[self] = height.constant
//        }
//        var textHeight = textView.text.heightWithConstrainedWidth(width: textView.width, font: textView.font ?? .systemFont(ofSize: 15))
//        //print(textHeight)
//        if textHeight > 300 {
//            textHeight = 300
//        }
//        if textHeight <= textViewsHeight[self] ?? 60 {
//            height.constant = textViewsHeight[self] ?? 60
//        } else {
//            height.constant = textHeight + 20
//        }
//    }
//}

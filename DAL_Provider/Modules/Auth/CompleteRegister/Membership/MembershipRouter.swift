//
//  MembershipRouter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class MembershipRouter: Router {
    typealias PresentingView = MembershipVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension MembershipRouter: MembershipRouterContract {
    func uploadDocument() {
        guard let scene = R.storyboard.uploadDocumentStoryboard.uploadDocumentVC() else { return }
        view?.push(scene)
    }
}

//
//  MembershipVC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class MembershipVC: BaseController {
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var periodLbl: UILabel!
    @IBOutlet weak var bankRadioBtn: RadioButton!
    var presenter: MembershipPresenter?
    var router: MembershipRouter?
    var bankCheck: Bool = false
    
}

// MARK: - ...  LifeCycle
extension MembershipVC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension MembershipVC {
    func setup() {
        
        bankRadioBtn.onSelect { [weak self] in
            self?.bankCheck = true
        }
        bankRadioBtn.onDeselect { [weak self] in
            self?.bankCheck = false
        }
        
        bankRadioBtn.select()
    }
}
extension MembershipVC {
    @IBAction func bankTransfer(_ sender: Any) {
        bankCheck ? bankRadioBtn.deselect() : bankRadioBtn.select()
    }
    @IBAction func pay(_ sender: Any) {
        if bankCheck == true {
            router?.uploadDocument()
        }
    }
}
// MARK: - ...  View Contract
extension MembershipVC: MembershipViewContract {
}

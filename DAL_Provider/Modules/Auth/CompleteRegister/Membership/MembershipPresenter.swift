//
//  MembershipPresenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class MembershipPresenter: BasePresenter<MembershipViewContract> {
}
// MARK: - ...  Presenter Contract
extension MembershipPresenter: MembershipPresenterContract {
}
// MARK: - ...  Example of network response
extension MembershipPresenter {
    func fetchResponse<T: MembershipModel>(response: NetworkResponse<T>) {
        switch response {
            case .success(_):
                break
            case .failure(_):
                break
        }
    }
}

//
//  MembershipContract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/4/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol MembershipPresenterContract: PresenterProtocol {
}
// MARK: - ...  View Contract
protocol MembershipViewContract: PresentingViewProtocol {
}
// MARK: - ...  Router Contract
protocol MembershipRouterContract: Router, RouterProtocol {
    func uploadDocument()
}

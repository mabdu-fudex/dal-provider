//
//  ForgetPasswordPresenter.swift
//  DAL_IOS
//
//  Created by M.abdu on 12/28/20.
//  Copyright © 2020 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class ForgetPasswordPresenter: BasePresenter<ForgetPasswordViewContract> {
}
// MARK: - ...  Presenter Contract
extension ForgetPasswordPresenter: ForgetPasswordPresenterContract {
    func sendCode(mobile: String?) {
        if mobile?.count ?? 0 != 9 {
            view?.didError(error: "The mobile number must be 9 digits".localized)
            return
        }
        NetworkManager.instance.paramaters["phone"] = mobile
        NetworkManager.instance.request(.resendCode, type: .post, BaseModel<Int>.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didSend(code: model?.data)
                case .failure(let error):
                    self?.view?.didError(error: error?.localizedDescription)
            }
        }
    }
}
// MARK: - ...  Example of network response
extension ForgetPasswordPresenter {

}

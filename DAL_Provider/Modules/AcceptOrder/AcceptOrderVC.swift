//
//  AcceptOrderVC.swift
//  DAL_Provider
//
//  Created by Mabdu on 28/02/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class AcceptOrderVC: BaseController {
    @IBOutlet weak var remainingTxf: UITextField!
    var presenter: AcceptOrderPresenter?
    var router: AcceptOrderRouter?
    var type: OrderType = .order
    var order: OrdersModel.Datum?
    var reservation: ReservationsModel.Datum?
    weak var delegate: AcceptOrderDelegate?
}

// MARK: - ...  LifeCycle
extension AcceptOrderVC {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension AcceptOrderVC {
    func setup() {
    }
    override func backBtn(_ sender: Any) {
        router?.didDismiss()
    }
    @IBAction func send(_ sender: Any) {
        if !validate(txfs: [remainingTxf]) {
            return
        }
        startLoading()
        presenter?.accept(type: type, id: reservation?.id ?? order?.id, remaining: remainingTxf.text)
    }
    @IBAction func cancel(_ sender: Any) {
        router?.didDismiss()
    }
}
// MARK: - ...  View Contract
extension AcceptOrderVC: AcceptOrderViewContract {
    func didAccept() {
        stopLoading()
        router?.didAccept()
    }
}

//
//  AcceptOrderPresenter.swift
//  DAL_Provider
//
//  Created by Mabdu on 28/02/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class AcceptOrderPresenter: BasePresenter<AcceptOrderViewContract> {
}
// MARK: - ...  Presenter Contract
extension AcceptOrderPresenter: AcceptOrderPresenterContract {
    func accept(type: OrderType, id: Int?, remaining: String?) {
        var method = ""
        if type == .reservation {
            method = NetworkConfigration.EndPoint.acceptReservation.rawValue
            NetworkManager.instance.paramaters["reservation_id"] = id
        } else {
            method = NetworkConfigration.EndPoint.acceptOrder.rawValue
            NetworkManager.instance.paramaters["order_id"] = id
        }
        NetworkManager.instance.paramaters["remaining"] = remaining
        NetworkManager.instance.request(method, type: .post, AcceptOrderModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didAccept()
                case .failure:
                    break
            }
        }
    }
}


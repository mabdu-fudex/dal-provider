//
//  AcceptOrderRouter.swift
//  DAL_Provider
//
//  Created by Mabdu on 28/02/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class AcceptOrderRouter: Router {
    typealias PresentingView = AcceptOrderVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension AcceptOrderRouter: AcceptOrderRouterContract {
    func didDismiss() {
        view?.dismiss(animated: true, completion: nil)
    }
    func didAccept() {
        view?.dismiss(animated: true, completion: {
            if self.view?.type == .reservation {
                self.view?.delegate?.acceptOrder(self.view?.delegate, reservation: self.view?.reservation)
            } else {
                self.view?.delegate?.acceptOrder(self.view?.delegate, order: self.view?.order)
            }
        })
    }
}

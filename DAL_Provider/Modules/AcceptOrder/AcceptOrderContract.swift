//
//  AcceptOrderContract.swift
//  DAL_Provider
//
//  Created by Mabdu on 28/02/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol AcceptOrderPresenterContract: PresenterProtocol {
    func accept(type: OrderType, id: Int?, remaining: String?)
}
// MARK: - ...  View Contract
protocol AcceptOrderViewContract: PresentingViewProtocol {
    func didAccept()
}
// MARK: - ...  Router Contract
protocol AcceptOrderRouterContract: Router, RouterProtocol {
    func didDismiss()
    func didAccept()
}

enum OrderType {
    case reservation
    case order
}
protocol AcceptOrderDelegate: class {
    func acceptOrder(_ delegate: AcceptOrderDelegate?, order: OrdersModel.Datum?)
    func acceptOrder(_ delegate: AcceptOrderDelegate?, reservation: ReservationsModel.Datum?)
}

extension AcceptOrderDelegate {
    func acceptOrder(_ delegate: AcceptOrderDelegate?, order: OrdersModel.Datum?) {
        
    }
    func acceptOrder(_ delegate: AcceptOrderDelegate?, reservation: ReservationsModel.Datum?) {
        
    }
}

//
//  CategoryStoresModel.swift
//  Mutsawiq
//
//  Created by M.abdu on 11/8/20.
//  Copyright © 2020 com.Rowaad. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class EditTagsView: UIView {
   
    @IBInspectable public var titleView: String {
        get {
            return self.titleView
        }
        set {
            self.placeHolderLbl.text = newValue.localized
        }
    }
   
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var placeHolderLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var TagCollection: UICollectionView!
    
    var editable: Bool = false
    var onlyItemSelected: Bool = false
    var selectedItemsPaths: [Int] = []
    var selectedItemsTags: [TagModel] = []
    weak var delegate: EditViewDelegate?
    weak var dataSource: TagCollectionViewDataSource? {
        didSet {
            self.reload()
        }
    }
    var source: [TagModel] {
        return dataSource?.tagCollectionView(for: self) ?? []
    }
    var view: BaseController? {
        get {
            let view = dataSource as? BaseController
            return view
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        initNib()
        updateView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initNib()
        updateView()
    }
    
    func initNib() {
        let bundle = Bundle(for: EditTagsView.self)
        bundle.loadNibNamed("EditTagsView", owner: self, options: nil)
        addSubview(container)
        container.frame = bounds
        container.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|", options: [], metrics: nil, views: ["childView": container ?? UIView()]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|", options: [], metrics: nil, views: ["childView": container ?? UIView()]))
    }

    @IBAction func edit(_ sender: Any) {
        editable = !editable
        if !editable {
            delegate?.editView(didSave: self)
        }
        updateView()
    }
}

extension EditTagsView {
    func updateView() {
        if editable {
            placeHolderLbl.UIViewAction { [weak self] in
                self?.didOpenTags("")
            }
            TagCollection.UIViewAction { [weak self] in
                self?.didOpenTags("")
            }
            TagCollection.isUserInteractionEnabled = true
            editBtn.setTitle("SAVE".localized, for: .normal)
        } else {
            placeHolderLbl.UIViewAction {
            
            }
            TagCollection.UIViewAction {

            }
            TagCollection.isUserInteractionEnabled = false
            editBtn.setTitle("EDIT".localized, for: .normal)
        }
        TagCollection.delegate = self
        TagCollection.dataSource = self
    }
    func reload() {
        TagCollection.reloadData()
        reloadHeights()
    }
    func reloadHeights() {
        if let constraint = container.constraints.first(where: { $0.firstAttribute == .height }) {
            if selectedItemsTags.count > 0 {
                constraint.constant -= 54
                constraint.constant += TagCollection.collectionViewLayout.collectionViewContentSize.height
                //constraint.constant = 105
            }
        }
    }
    @IBAction func didOpenTags(_ sender: Any) {
        if !editable {
            return
        }
        if selectedItemsTags.count == source.count {
            return
        }
        guard let scene = R.storyboard.pickerViewHelper.pickerController() else { return }
        scene.source = source
        scene.titleClosure = { [unowned self] row in
            return self.source[row].name
        }
        scene.didSelectClosure = { row in
            if self.onlyItemSelected {
                self.selectedItemsTags.removeAll()
                self.selectedItemsPaths.removeAll()
            }
            let item = self.source[row]
            if !self.selectedItemsPaths.contains(row) {
                self.selectedItemsPaths.append(row)
                self.selectedItemsTags.append(item)
                self.reload()
            }
        }
        self.view?.pushPop(scene)
    }
}

extension EditTagsView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text = selectedItemsTags[indexPath.row].name
        var textWidth = (text?.widthWithConstrainedWidth(width: collectionView.width, font: ThemeApp.Fonts.regularFont(size: 14)) ?? 40)
        if textWidth < 40 {
            textWidth = 40
        }
        let width = 40 + textWidth
        return CGSize(width: width, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedItemsTags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.cell(type: TagCollectionCell.self, indexPath)
        cell.tagNameLbl.text = selectedItemsTags[indexPath.row].name
        cell.delegate = self
        return cell
    }
}
extension EditTagsView: TagCollectionCellDelegate {
    func removeTag(path: Int) {
        self.selectedItemsPaths.remove(at: path)
        self.selectedItemsTags.remove(at: path)
        self.reload()
    }
}

//
//  AboutUsModel.swift
//  DAL_Provider
//
//  Created by Mabdu on 02/03/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Entity
class AboutUsModel: Codable {
    let status: Bool?
    let data: DataClass?
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let tax, service, email, phone, about_us, address: String?
        let facebook, snabchat, instagram: String?
    }

}

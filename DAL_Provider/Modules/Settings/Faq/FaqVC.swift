//
//  FaqVC.swift
//  DAL_Provider
//
//  Created by Mabdu on 02/03/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class FaqVC: BaseController {
    @IBOutlet weak var faqTbl: UITableView!
    var presenter: FaqPresenter?
    var router: FaqRouter?
    var questions: [FaqModel.Datum] = []
    var expandablePath: Int?
}

// MARK: - ...  LifeCycle
extension FaqVC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension FaqVC {
    func setup() {
        startLoading()
        presenter?.questions()
        faqTbl.delegate = self
        faqTbl.dataSource = self
    }
}
// MARK: - ...  View Contract
extension FaqVC: FaqViewContract {
    func didFetch(for list: [FaqModel.Datum]?) {
        stopLoading()
        questions.removeAll()
        questions.append(contentsOf: list ?? [])
        faqTbl.reloadData()
    }
}

extension FaqVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: FaqCell.self, indexPath)
        if expandablePath == indexPath.row {
            cell.isExpanded = true
        } else {
            cell.isExpanded = false
        }
        cell.model = questions[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if expandablePath == indexPath.row {
            expandablePath = nil
        } else {
            expandablePath = indexPath.row
        }
        FaqCell.COUNT_COLOR = 1
        tableView.reloadData()
    }
}

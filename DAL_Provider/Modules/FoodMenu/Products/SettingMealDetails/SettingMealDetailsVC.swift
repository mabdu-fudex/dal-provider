//
//  SettingMealDetailsVC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class SettingMealDetailsVC: BaseController {
    @IBOutlet weak var hideBtn: GradientButton!
    var presenter: SettingMealDetailsPresenter?
    var router: SettingMealDetailsRouter?
    weak var delegate: SettingsMealDelegate?
    var meal: ListProductsModel.Datum?
}

// MARK: - ...  LifeCycle
extension SettingMealDetailsVC {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension SettingMealDetailsVC {
    func setup() {
        if meal?.active == 1 {
            hideBtn.setTitle("HIDE".localized, for: .normal)
        } else {
            hideBtn.setTitle("SHOW".localized, for: .normal)
        }
    }
}
extension SettingMealDetailsVC {
    override func backBtn(_ sender: Any) {
        router?.didDismiss()
    }
    @IBAction func edit(_ sender: Any) {
        router?.didEdit()
    }
    @IBAction func hide(_ sender: Any) {
        router?.didHide()
    }
    @IBAction func remove(_ sender: Any) {
        router?.didDelete()
    }
}
// MARK: - ...  View Contract
extension SettingMealDetailsVC: SettingMealDetailsViewContract {
}

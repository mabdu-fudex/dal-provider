//
//  SettingMealDetailsPresenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class SettingMealDetailsPresenter: BasePresenter<SettingMealDetailsViewContract> {
}
// MARK: - ...  Presenter Contract
extension SettingMealDetailsPresenter: SettingMealDetailsPresenterContract {
}
// MARK: - ...  Example of network response
extension SettingMealDetailsPresenter {
    func fetchResponse<T: SettingMealDetailsModel>(response: NetworkResponse<T>) {
        switch response {
            case .success(_):
                break
            case .failure(_):
                break
        }
    }
}

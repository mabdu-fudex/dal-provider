//
//  SettingMealDetailsRouter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class SettingMealDetailsRouter: Router {
    typealias PresentingView = SettingMealDetailsVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension SettingMealDetailsRouter: SettingMealDetailsRouterContract {
    func didEdit() {
        view?.dismiss(animated: true, completion: {
            self.view?.delegate?.settingsMeal(self.view?.delegate, didEdit: self.view?.meal)
        })
    }
    
    func didDelete() {
        view?.dismiss(animated: true, completion: {
            self.view?.delegate?.settingsMeal(self.view?.delegate, didDelete: self.view?.meal)
        })
    }
    
    func didHide() {
        view?.dismiss(animated: true, completion: {
            self.view?.delegate?.settingsMeal(self.view?.delegate, didHide: self.view?.meal)
        })
    }
    
    func didDismiss() {
        view?.dismiss(animated: true, completion: nil)
    }
    
    
}

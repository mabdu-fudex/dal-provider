//
//  SettingMealDetailsContract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol SettingMealDetailsPresenterContract: PresenterProtocol {
}
// MARK: - ...  View Contract
protocol SettingMealDetailsViewContract: PresentingViewProtocol {
}
// MARK: - ...  Router Contract
protocol SettingMealDetailsRouterContract: Router, RouterProtocol {
    func didEdit()
    func didDelete()
    func didHide()
    func didDismiss()
}
protocol SettingsMealDelegate: class {
    func settingsMeal(_ delegate: SettingsMealDelegate?, didEdit meal: ListProductsModel.Datum?)
    func settingsMeal(_ delegate: SettingsMealDelegate?, didDelete meal: ListProductsModel.Datum?)
    func settingsMeal(_ delegate: SettingsMealDelegate?, didHide meal: ListProductsModel.Datum?)
}

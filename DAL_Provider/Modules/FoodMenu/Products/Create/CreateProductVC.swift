//
//  CreateProductVC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class CreateProductVC: BaseController {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var nameTxf: UITextField!
    @IBOutlet weak var priceTxf: UITextField!
    @IBOutlet weak var caloriesTxf: UITextField!
    @IBOutlet weak var mealTxf: UITextField!
    @IBOutlet weak var imageBtn: UIButton!
    @IBOutlet weak var extrasTbl: UITableView!
    @IBOutlet weak var extrasHeight: NSLayoutConstraint!
    @IBOutlet weak var hasExtraYesBtn: RadioButton!
    @IBOutlet weak var hasExtraNoBtn: RadioButton!
    var presenter: CreateProductPresenter?
    var router: CreateProductRouter?
    weak var delegate: CreateProductDelegate?
    weak var editDelegate: EditProductDelegate?
    var subCategory: SubCategoriesModel.Datum?
    var extras: [ExtrasListModel.Datum] {
        var array: [ExtrasListModel.Datum] = []
        for item in extrasList where item.isChecked == true {
            array.append(item)
        }
        return array
    }
    var extrasList: [ExtrasListModel.Datum] = []
    var hasExtras: Bool? {
        didSet {
            reload()
        }
    }
    var productImage: UIImage?
    var meal: ListProductsModel.Datum?
    lazy var imagePicker: GalleryPickerHelper? = {
        let picker = GalleryPickerHelper()
        picker.onPickImage = { [weak self] image in
            self?.imageBtn.setImage(image, for: .normal)
            self?.productImage = image
        }
        return picker
    }()
}

// MARK: - ...  LifeCycle
extension CreateProductVC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
        fetchExtras()
        reload()
        setupHasExtras()

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension CreateProductVC {
    func setup() {
        extrasTbl.delegate = self
        extrasTbl.dataSource = self
    }
    func fetchExtras() {
        startLoading()
        presenter?.fetchExtras()
    }
    func reload() {
        if hasExtras == true {
            extrasTbl.isHidden = false
            extrasTbl.reloadData()
            extrasHeight.constant = extrasTbl.contentSize.height
        } else {
            extrasTbl.isHidden = true
            //extrasTbl.reloadData()
            extrasHeight.constant = 0
        }
        if meal != nil {
            setupOldMeal()
        }
    }
    func setupOldMeal() {
        titleLbl.text = meal?.name
        nameTxf.text = meal?.name
        mealTxf.text = meal?.details
        caloriesTxf.text = meal?.calory
        priceTxf.text = meal?.price?.string
        imageBtn.setImage(url: meal?.img)
        for item in meal?.additions ?? [] {
            for index in 0..<extrasList.count {
                if item.id == extrasList[index].id {
                    extrasList[index].isOld = true
                    extrasList[index].isChecked = true
                } else {
                    extrasList[index].isOld = false
                    extrasList[index].isChecked = false
                }
            }
        }
        extrasTbl.reloadData()
        extrasHeight.constant = extrasTbl.contentSize.height
    }
    func setupHasExtras() {
        hasExtraYesBtn.onSelect { [weak self] in
            self?.hasExtras = true
            self?.hasExtraNoBtn.deselect()
        }
        hasExtraYesBtn.onDeselect { [weak self] in
            if self?.hasExtras == true {
                self?.hasExtras = nil
            }
        }
        hasExtraNoBtn.onSelect { [weak self] in
            self?.hasExtras = false
            self?.hasExtraYesBtn.deselect()
        }
        hasExtraNoBtn.onDeselect { [weak self] in
            if self?.hasExtras == false {
                self?.hasExtras = nil
            }
        }
    }
}
extension CreateProductVC {
    @IBAction func productImage(_ sender: Any) {
        imagePicker?.pick(in: self)
    }
    @IBAction func create(_ sender: Any) {
        if !validate(txfs: [nameTxf, mealTxf, priceTxf, caloriesTxf]) {
            return
        }
        if productImage == nil && meal == nil {
            openConfirmation(title: "Please select the product image".localized)
            return
        }
        startLoading()
        presenter?.createProduct()
    }
}
// MARK: - ...  View Contract
extension CreateProductVC: CreateProductViewContract, PopupConfirmationViewContract {
    func didCreateProduct(for product: ListProductsModel.Datum?) {
        stopLoading()
        var text = "The product has been added successfully".localized
        if meal != nil {
            text = "The product has been edited successfully".localized
        }
        openConfirmation(title: text, onSkipClosure: weak({ (strongSelf) in
            strongSelf.delegate?.createProduct(strongSelf.delegate, product: product)
            strongSelf.editDelegate?.editProduct(strongSelf.editDelegate, product: product)
            strongSelf.navigationController?.popViewController()
        }))
    }
    func didError(error: String?) {
        stopLoading()
        openConfirmation(title: error)
    }
    func didFetchExtras(for list: [ExtrasListModel.Datum]?) {
        stopLoading()
        extrasList.removeAll()
        extrasList.append(contentsOf: list ?? [])
        extrasList.append(.init())
        reload()
        
    }
}

extension CreateProductVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return extrasList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: ExtraTableCell.self, indexPath)
        if indexPath.row + 1 == extrasList.count {
            cell.isPlus = true
        } else {
            cell.isPlus = false
        }
        cell.model = extrasList[safe: indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ExtraTableCell
        if cell.isPlus == true {
            router?.createExtra()
        } else {
            extrasList[indexPath.row].isChecked = !(extrasList[indexPath.row].isChecked ?? false)
            reload()
        }
    }
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
//        if indexPath.row == extras.count {
//            return nil
//        }
//        if Localizer.current == .english {
//            guard orientation == .right else { return nil }
//        } else {
//            guard orientation == .left else { return nil }
//        }
//        let action = SwipeAction(style: .default, title: "".localized) { [weak self] (action, path) in
//            self?.extras.remove(at: indexPath.row)
//            self?.reload()
//        }
//        action.image = UIImage(named: "trash")
//        action.backgroundColor = R.color.background()
//        return [action]
//    }
}

extension CreateProductVC: CreateExtraDelegate {
    func createExtra(_ delegate: CreateExtraDelegate?, for extra: CreateExtraModel?) {
        fetchExtras()
    }
}

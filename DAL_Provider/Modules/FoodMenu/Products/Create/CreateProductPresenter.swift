//
//  CreateProductPresenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit
// MARK: - ...  Presenter
class CreateProductPresenter: BasePresenter<CreateProductViewContract> {
}
// MARK: - ...  Presenter Contract
extension CreateProductPresenter: CreateProductPresenterContract {
    func fetchExtras() {
        NetworkManager.instance.request(.extras, type: .get, ExtrasListModel.self) { [weak self] (response) in
           switch response {
               case .success(let model):
                self?.view?.didFetchExtras(for: model?.data)
               case .failure:
                   break
           }
       }
    }
    func createProduct() {
        guard let view = view as? CreateProductVC else { return }
        var paramters: [String: Any] = [:]
        paramters["category_id"] = view.subCategory?.categoryID
        paramters["sub_category_id"] = view.subCategory?.id
        paramters["name_ar"] = view.nameTxf.text
        paramters["name_en"] = view.nameTxf.text
        paramters["price"] = view.priceTxf.text
        paramters["calory"] = view.caloriesTxf.text
        paramters["details"] = view.mealTxf.text
        DateHelper.currentLocal = .english
        paramters["date"] = DateHelper().date(date: Date(), format: "yyyy-MM-dd")

        var extraImages: [String: UIImage] = [:]
        for index in 0..<view.extras.count {
            paramters["adds[\(index)][name_ar]"] = view.extras[index].name
            paramters["adds[\(index)][name_en]"] = view.extras[index].name
            paramters["adds[\(index)][price]"] = view.extras[index].price
            paramters["adds[\(index)][id]"] = view.extras[index].id
        }
        var method = ""
        if view.meal != nil {
            paramters["category_id"] = view.meal?.categoryID
            paramters["sub_category_id"] = view.meal?.subCategoryID
            method = NetworkConfigration.EndPoint.endPoint(point: .product, paramters: [view.meal?.id ?? 0])
        } else {
            method = NetworkConfigration.EndPoint.endPoint(point: .product, paramters: [])
        }
        NetworkManager.instance.paramaters = paramters
        NetworkManager.instance.uploadMultiImagesWithKey(method, type: .post, files: extraImages, file: ["img": view.productImage], CreateProductModel.self) { [weak self] (response) in
            self?.view?.stopLoading()
            switch response {
                case .success(let model):
                    self?.view?.didCreateProduct(for: model?.data)
                case .failure(let error):
                    print(error?.localizedDescription)
            }
        }
    }
}
// MARK: - ...  Example of network response
extension CreateProductPresenter {
}

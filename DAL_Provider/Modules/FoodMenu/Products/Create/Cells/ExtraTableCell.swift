//
//  ExtraTableCell.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import UIKit

class ExtraTableCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var selectView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var plusLbl: UILabel!
    @IBOutlet weak var checkMark: UIImageView!
    
    var isPlus: Bool = false
    func setup() {
        guard let model = model as? ExtrasListModel.Datum else { return }
        if isPlus {
            setupCreateExtra()
        } else {
            selectView.cornerRadius = 6
            selectView.backgroundColor = UIColor(red: 233/255, green: 233/255, blue: 240/255, alpha: 1)
            selectView.borderWidth = 1
            selectView.borderColor = R.color.borderColor1()
            titleLbl.text = model.name
            plusLbl.isHidden = true
            isPlus = false
            if model.isChecked == true {
                selectView.backgroundColor = R.color.secondColor()
                selectView.borderWidth = 0
                checkMark.isHidden = false
            } else {
                selectView.backgroundColor = UIColor(red: 233/255, green: 233/255, blue: 240/255, alpha: 1)
                selectView.borderWidth = 1
                selectView.borderColor = R.color.borderColor1()
                checkMark.isHidden = true
            }
        }
       
    }
    func setupCreateExtra() {
        isPlus = true
        selectView.cornerRadius = 6
        selectView.backgroundColor = .clear
        selectView.borderWidth = 1
        selectView.borderColor = R.color.mainColor()
        titleLbl.text = "Add other items".localized
        plusLbl.isHidden = false
    }
}

//
//  CreateProductContract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol CreateProductPresenterContract: PresenterProtocol {
    func fetchExtras() 
    func createProduct()
}
// MARK: - ...  View Contract
protocol CreateProductViewContract: PresentingViewProtocol {
    func didCreateProduct(for product: ListProductsModel.Datum?)
    func didError(error: String?)
    func didFetchExtras(for list: [ExtrasListModel.Datum]?)
}
// MARK: - ...  Router Contract
protocol CreateProductRouterContract: Router, RouterProtocol {
    func createExtra()
}

protocol CreateProductDelegate: class {
    func createProduct(_ delegate: CreateProductDelegate?, product: ListProductsModel.Datum?)
}

protocol EditProductDelegate: class {
    func editProduct(_ delegate: EditProductDelegate?, product: ListProductsModel.Datum?)
}

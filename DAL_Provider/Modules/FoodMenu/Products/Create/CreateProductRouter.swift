//
//  CreateProductRouter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class CreateProductRouter: Router {
    typealias PresentingView = CreateProductVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension CreateProductRouter: CreateProductRouterContract {
    func createExtra() {
        guard let scene = R.storyboard.createExtraStoryboard.createExtraVC() else { return }
        scene.delegate = view
        view?.pushPop(scene)
    }
}

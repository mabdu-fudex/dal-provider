//
//  ProductTableCell.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import UIKit
import Cosmos
class ProductTableCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var currencyLbl: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var notActiveLbl: UILabel!
    
    func setup() {
        guard let model = model as? ListProductsModel.Datum else { return }
        productImage.setImage(url: model.img)
        nameLbl.text = model.name
        descLbl.text = model.details
        priceLbl.text = model.price?.string
        rateView.rating = model.rate ?? 0
        rateView.text = "(\(model.rate ?? 0)/5)"
        if model.active == 0 {
            notActiveLbl.isHidden = false
        } else {
            notActiveLbl.isHidden = true
        }
    }
    
}

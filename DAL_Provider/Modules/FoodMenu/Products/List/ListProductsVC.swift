//
//  ListProductsVC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class ListProductsVC: BaseController {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var productsTbl: UITableView!
    var presenter: ListProductsPresenter?
    var router: ListProductsRouter?
    var subCategory: SubCategoriesModel.Datum?
    var products: [ListProductsModel.Datum] = []
}

// MARK: - ...  LifeCycle
extension ListProductsVC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension ListProductsVC {
    func setup() {
        titleLbl.text = subCategory?.name
        productsTbl.delegate = self
        productsTbl.dataSource = self
        startLoading()
        presenter?.fetchProducts(for: subCategory?.id)
    }
}
extension ListProductsVC {
    @IBAction func addProduct(_ sender: Any) {
        router?.create()
    }
}
// MARK: - ...  View Contract
extension ListProductsVC: ListProductsViewContract, PopupConfirmationViewContract {
    func didFetchProducts(for list: [ListProductsModel.Datum]?) {
        stopLoading()
        products.removeAll()
        products.append(contentsOf: list ?? [])
        productsTbl.reloadData()
    }
    func didError(error: String?) {
        stopLoading()
        openConfirmation(title: error)
    }
}

extension ListProductsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: ProductTableCell.self, indexPath)
        cell.model = products[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        router?.mealDetails(for: products[indexPath.row])
    }
}

//
//  ListProductsPresenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class ListProductsPresenter: BasePresenter<ListProductsViewContract> {
}
// MARK: - ...  Presenter Contract
extension ListProductsPresenter: ListProductsPresenterContract {
    func fetchProducts(for category: Int?) {
        let method = NetworkConfigration.EndPoint.endPoint(point: .myProducts, paramters: [category ?? 0])
        NetworkManager.instance.request(method, type: .get, ListProductsModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFetchProducts(for: model?.data)
                case .failure(let error):
                    self?.view?.didError(error: error?.localizedDescription)
            }
        }
    }
}
// MARK: - ...  Example of network response
extension ListProductsPresenter {
   
}

//
//  ListProductsContract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol ListProductsPresenterContract: PresenterProtocol {
    func fetchProducts(for category: Int?)
}
// MARK: - ...  View Contract
protocol ListProductsViewContract: PresentingViewProtocol {
    func didFetchProducts(for list: [ListProductsModel.Datum]?)
    func didError(error: String?)
}
// MARK: - ...  Router Contract
protocol ListProductsRouterContract: Router, RouterProtocol {
    func create()
    func mealDetails(for meal: ListProductsModel.Datum?)
}

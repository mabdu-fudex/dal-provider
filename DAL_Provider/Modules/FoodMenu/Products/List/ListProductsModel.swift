//
//  ListProductsModel.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
// MARK: - ProductsModel
struct ListProductsModel: Codable {
    let status: Bool?
    let data: [Datum]?
    
    // MARK: - Datum
    struct Datum: Codable {
        let id, userID, categoryID, subCategoryID: Int?
        let name: String?
        let price: Double?
        let calory, details: String?
        let img: String?
        let date, categoryName, subcategoryName: String?
        let additions: [Addition]?
        let rate: Double?
        var active: Int?
        var rates: [Rate]?
        enum CodingKeys: String, CodingKey {
            case id
            case rate = "rating"
            case userID = "user_id"
            case categoryID = "category_id"
            case subCategoryID = "sub_category_id"
            case name, price, calory, details, img, date
            case categoryName = "category_name"
            case subcategoryName = "subcategory_name"
            case additions
            case active
            case rates
        }
    }
    
    // MARK: - Addition
    struct Addition: Codable {
        let id, productID: Int?
        let name: String?
        let price: Double?
        let img: String?
        enum CodingKeys: String, CodingKey {
            case id
            case productID = "product_id"
            case name, price, img
        }
    }
    // MARK: - Addition
    struct Rate: Codable {
        let id, productID: Int?
        let name: String?
        let comment: String?
        let rating: Double?
        let rateableType: String?
        let img: String?
        let date: String?
        let userID: Int?
        let userImage: String?
        let userName: String?
        enum CodingKeys: String, CodingKey {
            case id
            case userID = "user_id"
            case userImage = "user_img"
            case userName = "user_name"
            case rating
            case rateableType = "rateable_type"
            case productID = "rateable_id"
            case name, comment, img, date
        }
    }
}

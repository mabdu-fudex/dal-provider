//
//  ListProductsRouter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class ListProductsRouter: Router {
    typealias PresentingView = ListProductsVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension ListProductsRouter: ListProductsRouterContract {
    func create() {
        guard let scene = R.storyboard.createProductStoryboard.createProductVC() else { return }
        scene.subCategory = view?.subCategory
        view?.push(scene)
    }
    func mealDetails(for meal: ListProductsModel.Datum?) {
        guard let scene = R.storyboard.mealDetailsStoryboard.mealDetailsVC() else { return }
        scene.meal = meal
        scene.subCategoryTitle = view?.subCategory?.name
        view?.push(scene)
    }
}

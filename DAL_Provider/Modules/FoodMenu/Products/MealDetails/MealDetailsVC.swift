//
//  MealDetailsVC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit
import Cosmos
// MARK: - ...  ViewController - Vars
class MealDetailsVC: BaseController {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var currencyLbl: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var rateLbl: UILabel!
    @IBOutlet weak var caloriesLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var extrasTbl: UITableView!
    @IBOutlet weak var ratesTbl: UITableView!
    @IBOutlet weak var rateInnerStackView: UIView!
    @IBOutlet weak var rateSuperView: UIView!
    
    var presenter: MealDetailsPresenter?
    var router: MealDetailsRouter?
    var meal: ListProductsModel.Datum?
    var subCategoryTitle: String?
}

// MARK: - ...  LifeCycle
extension MealDetailsVC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension MealDetailsVC {
    func setup() {
        startLoading()
        presenter?.fetchMeal(for: meal?.id)
        titleLbl.text = subCategoryTitle
        extrasTbl.delegate = self
        extrasTbl.dataSource = self
        ratesTbl.delegate = self
        ratesTbl.dataSource = self
        rateInnerStackView.isHidden = true
        rateSuperView.isHidden = true
        reload()
    }
    func reload() {
        productImage.setImage(url: meal?.img)
        nameLbl.text = meal?.name
        caloriesLbl.text = "\("Calory".localized) \(meal?.calory ?? "")"
        descLbl.text = meal?.details
        priceLbl.text = meal?.price?.string
        if Localizer.current == .arabic {
            priceLbl.textAlignment = .left
        } else {
            priceLbl.textAlignment = .right
        }
        rateView.rating = meal?.rate ?? 0
        rateLbl.text = "(\(meal?.rate ?? 0)/5)"
        extrasTbl.reloadData()
        extrasTbl.layoutIfNeeded()
    }
    func reloadRates() {
        if meal?.rates?.count ?? 0 > 0 {
            rateInnerStackView.isHidden = false
            rateSuperView.isHidden = false
            ratesTbl.estimatedRowHeight = 500
            ratesTbl.isScrollEnabled = true
            ratesTbl.reloadData {
                self.ratesTbl.layoutIfNeeded()
            }
        }
        
        
    }
    func reloadHeights() {
        if let constraint = extrasTbl.constraints.first(where: { $0.firstAttribute == .height }) {
            constraint.constant = extrasTbl.contentSize.height
        }
        if let constraint = self.ratesTbl.constraints.first(where: { $0.firstAttribute == .height }) {
            constraint.constant = self.ratesTbl.contentSize.height + 5
        }
    }
}
extension MealDetailsVC {
    @IBAction func setting(_ sender: Any) {
        router?.settings()
    }
    @IBAction func showMoreRates(_ sender: Any) {
        router?.rates(for: meal)
    }
}
// MARK: - ...  View Contract
extension MealDetailsVC: MealDetailsViewContract, PopupConfirmationViewContract {
    func didFetch(for meal: ListProductsModel.Datum?) {
        stopLoading()
        self.meal = meal
        reloadRates()
    }
    func didDelete(message: String?) {
        stopLoading()
        openConfirmation(title: message, onSkipClosure: { [weak self] in
            self?.backBtn(self)
        })
    }
    func didHide(message: String?) {
        stopLoading()
        if meal?.active == 1 {
            meal?.active = 0
        } else {
            meal?.active = 1
        }
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
            self.openConfirmation(title: message)
        }
    }
    func didError(error: String?) {
        stopLoading()
        openConfirmation(title: error)
    }
}

extension MealDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == extrasTbl {
            return meal?.additions?.count ?? 0
        } else {
            return meal?.rates?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == extrasTbl {
            var cell = tableView.cell(type: MealExtraCell.self, indexPath)
            cell.model = meal?.additions?[safe: indexPath.row]
            return cell
        } else {
            var cell = tableView.cell(type: MealRateCell.self, indexPath)
            cell.model = meal?.rates?[safe: indexPath.row]
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        reloadHeights()
    }
}

extension MealDetailsVC: SettingsMealDelegate {
    func settingsMeal(_ delegate: SettingsMealDelegate?, didEdit meal: ListProductsModel.Datum?) {
        router?.editMeal(for: meal)
    }
    
    func settingsMeal(_ delegate: SettingsMealDelegate?, didDelete meal: ListProductsModel.Datum?) {
        startLoading()
        presenter?.deleteMeal(for: meal?.id)
    }
    
    func settingsMeal(_ delegate: SettingsMealDelegate?, didHide meal: ListProductsModel.Datum?) {
        startLoading()
        presenter?.hideMeal(for: meal?.id)
    }
}

extension MealDetailsVC: EditProductDelegate {
    func editProduct(_ delegate: EditProductDelegate?, product: ListProductsModel.Datum?) {
        self.meal = product
        self.reload()
    }
}
//class UIDynamicTableView: UITableView {
//    override var intrinsicContentSize: CGSize {
//        self.layoutIfNeeded()
//        return CGSize(width: UIView.noIntrinsicMetric, height: self.contentSize.height)
//    }
//
//    override func reloadData() {
//        super.reloadData()
//        self.invalidateIntrinsicContentSize()
//    }
//}

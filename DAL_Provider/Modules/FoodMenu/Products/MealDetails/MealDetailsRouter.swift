//
//  MealDetailsRouter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class MealDetailsRouter: Router {
    typealias PresentingView = MealDetailsVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension MealDetailsRouter: MealDetailsRouterContract {
    func settings() {
        guard let scene = R.storyboard.settingMealDetailsStoryboard.settingMealDetailsVC() else { return }
        scene.delegate = view
        scene.meal = view?.meal
        view?.pushPop(scene)
    }
    func rates(for meal: ListProductsModel.Datum?) {
        guard let scene = R.storyboard.showMoreRatesStoryboard.showMoreRatesVC() else { return }
        scene.meal = meal
        view?.pushPop(scene)
    }
    func editMeal(for meal: ListProductsModel.Datum?) {
        guard let scene = R.storyboard.createProductStoryboard.createProductVC() else { return }
        scene.meal = meal
        scene.editDelegate = view
        view?.push(scene)
    }
}

//
//  MealDetailsModel.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Entity
class MealDetailsModel: Codable {
    let status: Bool?
    let data: ListProductsModel.Datum?
    let msg: String?
}

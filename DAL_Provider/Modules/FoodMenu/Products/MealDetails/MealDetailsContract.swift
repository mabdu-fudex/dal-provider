//
//  MealDetailsContract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol MealDetailsPresenterContract: PresenterProtocol {
    func fetchMeal(for id: Int?)
    func deleteMeal(for id: Int?)
    func hideMeal(for id: Int?)
}
// MARK: - ...  View Contract
protocol MealDetailsViewContract: PresentingViewProtocol {
    func didFetch(for meal: ListProductsModel.Datum?)
    func didDelete(message: String?)
    func didHide(message: String?)
    func didError(error: String?)
}
// MARK: - ...  Router Contract
protocol MealDetailsRouterContract: Router, RouterProtocol {
    func settings()
    func rates(for meal: ListProductsModel.Datum?)
    func editMeal(for meal: ListProductsModel.Datum?)
}

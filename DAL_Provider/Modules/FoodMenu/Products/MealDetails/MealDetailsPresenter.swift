//
//  MealDetailsPresenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class MealDetailsPresenter: BasePresenter<MealDetailsViewContract> {
}
// MARK: - ...  Presenter Contract
extension MealDetailsPresenter: MealDetailsPresenterContract {
    func fetchMeal(for id: Int?) {
        let method = NetworkConfigration.EndPoint.endPoint(point: .product, paramters: [id ?? 0])
        NetworkManager.instance.request(method, type: .get, MealDetailsModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFetch(for: model?.data)
                case .failure(let error):
                    self?.view?.didError(error: error?.localizedDescription)
            }
        }
    }
    func deleteMeal(for id: Int?) {
        let method = NetworkConfigration.EndPoint.endPoint(point: .product, paramters: [id ?? 0])
        NetworkManager.instance.request(method, type: .delete, MealDetailsModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didDelete(message: model?.msg)
                case .failure(let error):
                    self?.view?.didError(error: error?.localizedDescription)
            }
        }
    }
    func hideMeal(for id: Int?) {
        let method = NetworkConfigration.EndPoint.endPoint(point: .hideProduct, paramters: [id ?? 0])
        NetworkManager.instance.request(method, type: .get, MealDetailsModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didHide(message: model?.msg)
                case .failure(let error):
                    self?.view?.didError(error: error?.localizedDescription)
            }
        }
    }
}
// MARK: - ...  Example of network response
extension MealDetailsPresenter {

}

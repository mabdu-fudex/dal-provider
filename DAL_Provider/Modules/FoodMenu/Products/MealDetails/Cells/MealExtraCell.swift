//
//  MealExtraCell.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import UIKit

class MealExtraCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var extraImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    func setup() {
        guard let model = model as? ListProductsModel.Addition else { return }
        if case model.img?.isEmpty = true {
            extraImage.setImage(url: "http://dev.fudexsb.com/demo/dal/public/uploads/category_132021")
        } else {
            extraImage.setImage(url: model.img)
        }
        nameLbl.text = model.name
        priceLbl.text = "\("Ryal".localized) \(model.price ?? 0)"
    }
}

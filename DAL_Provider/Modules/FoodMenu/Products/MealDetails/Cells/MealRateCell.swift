//
//  MealRateCell.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import UIKit

class MealRateCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    func setup() {
        guard let model = model as? ListProductsModel.Rate else { return }
        commentLbl.text = model.comment
        dateLbl.text = model.date
        userImage.setImage(url: model.userImage)
        usernameLbl.text = model.userName
    }
    
}

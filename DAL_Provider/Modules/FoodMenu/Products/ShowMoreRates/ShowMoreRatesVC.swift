//
//  ShowMoreRatesVC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class ShowMoreRatesVC: BaseController {
    @IBOutlet weak var ratesTbl: UITableView!
    var presenter: ShowMoreRatesPresenter?
    var router: ShowMoreRatesRouter?
    var meal: ListProductsModel.Datum?
}

// MARK: - ...  LifeCycle
extension ShowMoreRatesVC {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension ShowMoreRatesVC {
    func setup() {
        ratesTbl.delegate = self
        ratesTbl.dataSource = self
        ratesTbl.estimatedRowHeight = 500
        ratesTbl.reloadData {
            self.ratesTbl.layoutIfNeeded()
            self.ratesTbl.scrollToBottom()
        }
    }
    func reloadHeights() {
        if let constraint = self.ratesTbl.constraints.first(where: { $0.firstAttribute == .height }) {
            let height = self.ratesTbl.contentSize.height + 20
            if height >= (self.view.height - 200) {
                constraint.constant = self.view.height - 200
                self.ratesTbl.isScrollEnabled = true
            } else {
                constraint.constant = self.ratesTbl.contentSize.height + 20
                self.ratesTbl.isScrollEnabled = false
            }
        }
    }
    override func backBtn(_ sender: Any) {
        router?.didDismiss()
    }
}
// MARK: - ...  View Contract
extension ShowMoreRatesVC: ShowMoreRatesViewContract {
}
extension ShowMoreRatesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meal?.rates?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: MealRateCell.self, indexPath)
        cell.model = meal?.rates?[safe: indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        reloadHeights()
    }
}

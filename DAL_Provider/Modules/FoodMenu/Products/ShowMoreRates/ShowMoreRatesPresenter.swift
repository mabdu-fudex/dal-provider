//
//  ShowMoreRatesPresenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class ShowMoreRatesPresenter: BasePresenter<ShowMoreRatesViewContract> {
}
// MARK: - ...  Presenter Contract
extension ShowMoreRatesPresenter: ShowMoreRatesPresenterContract {
}
// MARK: - ...  Example of network response
extension ShowMoreRatesPresenter {
    func fetchResponse<T: ShowMoreRatesModel>(response: NetworkResponse<T>) {
        switch response {
            case .success(_):
                break
            case .failure(_):
                break
        }
    }
}

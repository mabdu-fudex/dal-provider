//
//  ShowMoreRatesContract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol ShowMoreRatesPresenterContract: PresenterProtocol {
}
// MARK: - ...  View Contract
protocol ShowMoreRatesViewContract: PresentingViewProtocol {
}
// MARK: - ...  Router Contract
protocol ShowMoreRatesRouterContract: Router, RouterProtocol {
    func didDismiss()
}

//
//  ShowMoreRatesRouter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/13/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class ShowMoreRatesRouter: Router {
    typealias PresentingView = ShowMoreRatesVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension ShowMoreRatesRouter: ShowMoreRatesRouterContract {
    func didDismiss() {
        view?.dismiss(animated: true, completion: nil)
    }
}

//
//  SubCategoriesContract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/10/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol SubCategoriesPresenterContract: PresenterProtocol {
    func fetchSubCategories()
    func deleteSubCategory(for id: Int)
    func hideMenu()
}
// MARK: - ...  View Contract
protocol SubCategoriesViewContract: PresentingViewProtocol {
    func didFetchSubCategories(list: [SubCategoriesModel.Datum]?)
    func didHide(for value: Int?)
    func didError(error: String?)
}
// MARK: - ...  Router Contract
protocol SubCategoriesRouterContract: Router, RouterProtocol {
    func create()
    func listProducts(for category: SubCategoriesModel.Datum?)
}

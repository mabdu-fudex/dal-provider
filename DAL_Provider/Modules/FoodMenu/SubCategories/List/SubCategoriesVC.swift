//
//  SubCategoriesVC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/10/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class SubCategoriesVC: BaseController {
    @IBOutlet weak var hideMenuSwitch: UISwitch!
    @IBOutlet weak var subCategoriesTbl: UITableView!
    var presenter: SubCategoriesPresenter?
    var router: SubCategoriesRouter?
    var subCategories: [SubCategoriesModel.Datum] = []
    var menuActive: Int = 0
}

// MARK: - ...  LifeCycle
extension SubCategoriesVC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension SubCategoriesVC {
    func setup() {
        subCategoriesTbl.delegate = self
        subCategoriesTbl.dataSource = self
        startLoading()
        presenter?.fetchSubCategories()
    }
}
extension SubCategoriesVC {
    @IBAction func hideMenu(_ sender: Any) {
        startLoading()
        presenter?.hideMenu()
    }
    @IBAction func addCategory(_ sender: Any) {
        router?.create()
    }
}
// MARK: - ...  View Contract
extension SubCategoriesVC: SubCategoriesViewContract, PopupConfirmationViewContract {
    func didFetchSubCategories(list: [SubCategoriesModel.Datum]?) {
        stopLoading()
        subCategories.removeAll()
        subCategories.append(contentsOf: list ?? [])
        subCategoriesTbl.reloadData()
    }
    func didHide(for value: Int? = nil) {
        stopLoading()
        if value == nil {
            if menuActive == 0 {
                menuActive = 1
            } else {
                menuActive = 0
            }
        } else {
            menuActive = value ?? 0
        }
        if menuActive == 0 {
            subCategoriesTbl.fadeOut()
            hideMenuSwitch.thumbTintColor = R.color.textColorBlue()
            hideMenuSwitch.isOn = true
        } else {
            subCategoriesTbl.fadeIn()
            hideMenuSwitch.thumbTintColor = R.color.mainColor()
            hideMenuSwitch.isOn = false
        }
    }
    func didError(error: String?) {
        openConfirmation(title: error)
    }
}

extension SubCategoriesVC: UITableViewDelegate, UITableViewDataSource, SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: SubCategoryCell.self, indexPath)
        cell.delegate = self
        cell.model = subCategories[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        if Localizer.current == .english {
            guard orientation == .right else { return nil }
        } else {
            guard orientation == .left else { return nil }
        }
        let action = SwipeAction(style: .default, title: "".localized) { (action, path) in
            self.presenter?.deleteSubCategory(for: self.subCategories[path.row].id ?? 0)
            self.subCategories.remove(at: indexPath.row)
            self.subCategoriesTbl.reloadData()
        }
        action.image = UIImage(named: "trash")
        action.backgroundColor = R.color.background()
        return [action]
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        router?.listProducts(for: subCategories[indexPath.row])
    }
}

extension SubCategoriesVC: CreateSubCategoryDelegate {
    func didCreateSubCategory(category: SubCategoriesModel.Datum?) {
        guard let category = category else { return }
        subCategories.append(category)
        subCategoriesTbl.reloadData()
    }
}

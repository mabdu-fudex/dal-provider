//
//  SubCategoryCell.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/10/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import UIKit

class SubCategoryCell: SwipeTableViewCell, CellProtocol {
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var foodCountLbl: UILabel!
    
    func setup() {
        guard let model = model as? SubCategoriesModel.Datum else { return }
        categoryImage.setImage(url: model.img)
        nameLbl.text = model.name
        foodCountLbl.text = "\("Type available".localized) \(model.count ?? 0)"
    }
}

//
//  SubCategoriesRouter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/10/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class SubCategoriesRouter: Router {
    typealias PresentingView = SubCategoriesVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension SubCategoriesRouter: SubCategoriesRouterContract {
    func create() {
        guard let scene = R.storyboard.createSubCategoryStoryboard.createSubCategoryVC() else { return }
        scene.delegate = view
        view?.pushPop(scene)
    }
    func listProducts(for category: SubCategoriesModel.Datum?) {
        guard let scene = R.storyboard.listProductsStoryboard.listProductsVC() else { return }
        scene.subCategory = category
        view?.push(scene)
    }
}

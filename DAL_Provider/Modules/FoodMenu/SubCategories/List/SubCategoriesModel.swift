// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let subCategoryModel = try? newJSONDecoder().decode(SubCategoryModel.self, from: jsonData)

import Foundation

// MARK: - SubCategoryModel
struct SubCategoriesModel: Codable {
    let status: Bool?
    let data: [Datum]?
    let active: Int?
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let name: String?
        let img: String?
        let count: Int?
        let userID, active, categoryID: Int?
        let createdAt, updatedAt: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case name
            case count
            case img
            case userID = "user_id"
            case active
            case categoryID = "category_id"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
        }
    }

}

//
//  SubCategoriesPresenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/10/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class SubCategoriesPresenter: BasePresenter<SubCategoriesViewContract> {
}
// MARK: - ...  Presenter Contract
extension SubCategoriesPresenter: SubCategoriesPresenterContract {
    func fetchSubCategories() {
         NetworkManager.instance.request(.subCategories, type: .get, SubCategoriesModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFetchSubCategories(list: model?.data)
                    self?.view?.didHide(for: model?.active)
                case .failure:
                    break
            }
        }
    }
    func deleteSubCategory(for id: Int) {
        let method = NetworkConfigration.EndPoint.endPoint(point: .deleteSubCategory, paramters: [id])
        NetworkManager.instance.request(method, type: .delete, SubCategoriesModel.self) { [weak self] (response) in
            switch response {
                case .success:
                    break
                case .failure(let error):
                    self?.view?.didError(error: error?.localizedDescription)
            }
        }
    }
    func hideMenu() {
        NetworkManager.instance.request(.hideMenu, type: .get, SubCategoriesModel.self) { [weak self] (response) in
            switch response {
                case .success:
                    self?.view?.didHide(for: nil)
                case .failure(let error):
                    self?.view?.didError(error: error?.localizedDescription)
            }
        }
    }
}
// MARK: - ...  Example of network response
extension SubCategoriesPresenter {
 
}

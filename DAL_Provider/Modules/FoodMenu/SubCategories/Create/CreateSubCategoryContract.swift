//
//  CreateSubCategoryContract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/10/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit
// MARK: - ...  Presenter Contract
protocol CreateSubCategoryPresenterContract: PresenterProtocol {
    func createCategory(name: String?, category: Int?, image: UIImage?)
}
// MARK: - ...  View Contract
protocol CreateSubCategoryViewContract: PresentingViewProtocol {
    func didCreateCategory(category: SubCategoriesModel.Datum?)
}
// MARK: - ...  Router Contract
protocol CreateSubCategoryRouterContract: Router, RouterProtocol {
    func subCategories(category: SubCategoriesModel.Datum?)
}

protocol CreateSubCategoryDelegate: class {
    func didCreateSubCategory(category: SubCategoriesModel.Datum?)
}

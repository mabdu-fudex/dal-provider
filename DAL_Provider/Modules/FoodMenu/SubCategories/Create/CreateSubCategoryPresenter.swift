//
//  CreateSubCategoryPresenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/10/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit
// MARK: - ...  Presenter
class CreateSubCategoryPresenter: BasePresenter<CreateSubCategoryViewContract> {
}
// MARK: - ...  Presenter Contract
extension CreateSubCategoryPresenter: CreateSubCategoryPresenterContract {
    func createCategory(name: String?, category: Int?, image: UIImage?) {
        var paramters: [String: Any] = [:]
        paramters["name_ar"] = name
        paramters["name_en"] = name
        paramters["category_id"] = category
        NetworkManager.instance.paramaters = paramters
        let method = NetworkConfigration.EndPoint.endPoint(point: .createSubCategory, paramters: [])
        NetworkManager.instance.uploadMultiImages(method, type: .post, file: ["img": image], CreateSubCategoryModel.self) { [weak self] (response) in
            self?.view?.stopLoading()
            switch response {
                case .success(let model):
                    self?.view?.didCreateCategory(category: model?.data)
                case .failure:
                    break
             }
        }
    }
}

//
//  CreateSubCategoryVC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/10/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class CreateSubCategoryVC: BaseController {
    @IBOutlet weak var nameTxf: UITextField!
    @IBOutlet weak var classificationCollection: TagCollectionView!
    @IBOutlet weak var imageBtn: UIButton!
    var presenter: CreateSubCategoryPresenter?
    var router: CreateSubCategoryRouter?
    weak var delegate: CreateSubCategoryDelegate?
    var categoryImage: UIImage?
    lazy var imagePicker: GalleryPickerHelper? = {
        let picker = GalleryPickerHelper()
        picker.onPickImage = { [weak self] image in
            self?.imageBtn.setImage(image, for: .normal)
            self?.categoryImage = image
        }
        return picker
    }()
}

// MARK: - ...  LifeCycle
extension CreateSubCategoryVC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension CreateSubCategoryVC {
    func setup() {
        classificationCollection.delegate = self
        classificationCollection.dataSource = self
        classificationCollection.onlyItemSelected = true
    }
}

extension CreateSubCategoryVC {
    override func backBtn(_ sender: Any) {
        router?.subCategories()
    }
    @IBAction func categoryImage(_ sender: Any) {
        imagePicker?.pick(in: self)
    }
    @IBAction func create(_ sender: Any) {
        if categoryImage == nil {
            openConfirmation(title: "Please select the section image".localized)
            return
        }
        if !validate(txfs: [nameTxf]) {
            return
        }
        startLoading()
        let category = classificationCollection.selectedItemsTags.first?.id
        presenter?.createCategory(name: nameTxf.text, category: category, image: categoryImage)
    }
}
// MARK: - ...  View Contract
extension CreateSubCategoryVC: CreateSubCategoryViewContract, PopupConfirmationViewContract {
    func didCreateCategory(category: SubCategoriesModel.Datum?) {
        stopLoading()
        router?.subCategories(category: category)
    }
}

extension CreateSubCategoryVC: TagCollectionViewDataSource, TagCollectionViewDelegate {
    func tagCollectionView(for collection: TagCollectionView?) -> [TagModel] {
        return UserRoot.Category.transform(for: UserRoot.fetchUser()?.categories ?? [])
    }
}

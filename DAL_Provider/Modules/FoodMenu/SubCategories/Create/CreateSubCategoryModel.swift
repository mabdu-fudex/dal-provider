
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let subCategoryModel = try? newJSONDecoder().decode(SubCategoryModel.self, from: jsonData)

import Foundation

// MARK: - SubCategoryModel
struct CreateSubCategoryModel: Codable {
    let status: Bool?
    let data: SubCategoriesModel.Datum?
    let msg: String?
}

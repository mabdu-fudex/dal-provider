//
//  CreateSubCategoryRouter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/10/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class CreateSubCategoryRouter: Router {
    typealias PresentingView = CreateSubCategoryVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension CreateSubCategoryRouter: CreateSubCategoryRouterContract {
    func subCategories(category: SubCategoriesModel.Datum? = nil) {
        view?.delegate?.didCreateSubCategory(category: category)
        view?.dismiss(animated: true, completion: nil)
    }
}

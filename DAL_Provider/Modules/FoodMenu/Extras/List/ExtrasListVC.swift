//
//  ExtrasListVC.swift
//  DAL_Provider
//
//  Created by Mabdu on 03/03/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class ExtrasListVC: BaseController {
    @IBOutlet weak var extrasTbl: UITableView!
    var presenter: ExtrasListPresenter?
    var router: ExtrasListRouter?
    var extras: [ExtrasListModel.Datum] = []
}

// MARK: - ...  LifeCycle
extension ExtrasListVC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension ExtrasListVC {
    func setup() {
        extrasTbl.delegate = self
        extrasTbl.dataSource = self
        fetch()
    }
    func fetch() {
        startLoading()
        presenter?.fetchExtras()
    }
    @IBAction func createExtra(_ sender: Any) {
        router?.createExtra()
    }
}
// MARK: - ...  View Contract
extension ExtrasListVC: ExtrasListViewContract {
    func didFetch(for list: [ExtrasListModel.Datum]?) {
        extras.removeAll()
        extras.append(contentsOf: list ?? [])
        extrasTbl.reloadData()
        stopLoading()
    }
    func didDelete() {
        
    }
}
extension ExtrasListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return extras.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: ExtraCell.self, indexPath)
        cell.delegate = self
        cell.model = extras[indexPath.row]
        return cell
    }
    
    
}

extension ExtrasListVC: CreateExtraDelegate {
    func createExtra(_ delegate: CreateExtraDelegate?, for extra: CreateExtraModel?) {
        fetch()
    }
}

extension ExtrasListVC: ExtraCellDelegate {
    func extraCell(for cell: ExtraCell) {
        let extra = extras[cell.path ?? 0]
        presenter?.deleteExtra(for: extra.id)
        extras.remove(at: cell.path ?? 0)
        extrasTbl.reloadData()
    }
}

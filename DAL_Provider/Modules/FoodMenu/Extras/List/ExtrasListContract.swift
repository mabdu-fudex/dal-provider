//
//  ExtrasListContract.swift
//  DAL_Provider
//
//  Created by Mabdu on 03/03/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol ExtrasListPresenterContract: PresenterProtocol {
    func fetchExtras()
    func deleteExtra(for id: Int?)
}
// MARK: - ...  View Contract
protocol ExtrasListViewContract: PresentingViewProtocol {
    func didFetch(for list: [ExtrasListModel.Datum]?)
    func didDelete()
}
// MARK: - ...  Router Contract
protocol ExtrasListRouterContract: Router, RouterProtocol {
    func createExtra()
}

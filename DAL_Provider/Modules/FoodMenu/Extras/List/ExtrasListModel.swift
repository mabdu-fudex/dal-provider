//
//  ExtrasListModel.swift
//  DAL_Provider
//
//  Created by Mabdu on 03/03/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Entity
class ExtrasListModel: Codable {
    let status: Bool?
    let data: [Datum]?
    
    // MARK: - Datum
    struct Datum: Codable {
        var id: Int?
        var productID: Int?
        var name: String?
        var price: Double?
        var img: String?
        var isChecked: Bool?
        var isOld: Bool?
        var image: String?
        enum CodingKeys: String, CodingKey {
            case id
            case productID = "product_id"
            case name, price, img, isChecked, isOld, image
        }
    }

}

//
//  ExtrasListRouter.swift
//  DAL_Provider
//
//  Created by Mabdu on 03/03/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class ExtrasListRouter: Router {
    typealias PresentingView = ExtrasListVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension ExtrasListRouter: ExtrasListRouterContract {
    func createExtra() {
        guard let scene = R.storyboard.createExtraStoryboard.createExtraVC() else { return }
        scene.delegate = view
        view?.pushPop(scene)
    }
}

//
//  ExtraCell.swift
//  DAL_Provider
//
//  Created by Mabdu on 03/03/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import UIKit

protocol ExtraCellDelegate: class {
    func extraCell(for cell: ExtraCell)
}
class ExtraCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var removeImage: UIImageView!
    
    weak var delegate: ExtraCellDelegate?
    func setup() {
        guard let model = model as? ExtrasListModel.Datum else { return }
        titleLbl.text = model.name
        priceLbl.text = "\(model.price?.string ?? "") \("SAR.short".localized)"
    }
    @IBAction func remove(_ sender: Any) {
        delegate?.extraCell(for: self)
    }
}

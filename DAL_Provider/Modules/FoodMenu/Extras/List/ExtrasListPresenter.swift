//
//  ExtrasListPresenter.swift
//  DAL_Provider
//
//  Created by Mabdu on 03/03/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class ExtrasListPresenter: BasePresenter<ExtrasListViewContract> {
}
// MARK: - ...  Presenter Contract
extension ExtrasListPresenter: ExtrasListPresenterContract {
    func fetchExtras() {
        NetworkManager.instance.paramaters["user_id"] = UserRoot.fetchUser()?.id
        NetworkManager.instance.request(.extras, type: .get, ExtrasListModel.self) { [weak self] (response) in
           switch response {
               case .success(let model):
                self?.view?.didFetch(for: model?.data)
               case .failure:
                   break
           }
       }
    }
    func deleteExtra(for id: Int?) {
        let method = NetworkConfigration.EndPoint.endPoint(point: .extra, paramters: [id ?? 0])
        NetworkManager.instance.request(method, type: .delete, ExtrasListModel.self) { [weak self] (response) in
           switch response {
               case .success(let model):
                self?.view?.didDelete()
               case .failure:
                   break
           }
       }
    }
}

//
//  CreateExtraRouter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class CreateExtraRouter: Router {
    typealias PresentingView = CreateExtraVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension CreateExtraRouter: CreateExtraRouterContract {
    func didCreate(for extra: CreateExtraModel? = nil) {
        view?.delegate?.createExtra(view?.delegate, for: extra)
        view?.dismiss(animated: true, completion: nil)
    }
}

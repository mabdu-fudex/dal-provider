//
//  CreateExtraModel.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit
// MARK: - ...  Entity
class CreateExtraModel {
    var name: String?
    var price: Double?
    var image: UIImage?
    var isOld: Bool?
    var id: Int?
}


// MARK: - ...  Entity
class CreateExtraData: Codable {
    let status: Bool?
    let data: ExtrasListModel.Datum?
}

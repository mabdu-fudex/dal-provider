//
//  CreateExtraContract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol CreateExtraPresenterContract: PresenterProtocol {
    func create(extra: CreateExtraModel?)
}
// MARK: - ...  View Contract
protocol CreateExtraViewContract: PresentingViewProtocol {
    func didCreate()
    func didError(error: String?)
}
// MARK: - ...  Router Contract
protocol CreateExtraRouterContract: Router, RouterProtocol {
    func didCreate(for extra: CreateExtraModel?)
}

protocol CreateExtraDelegate: class {
    func createExtra(_ delegate: CreateExtraDelegate?, for extra: CreateExtraModel?)
}

//
//  CreateExtraVC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class CreateExtraVC: BaseController {
    @IBOutlet weak var nameTxf: UITextField!
    @IBOutlet weak var priceTxf: UITextField!
    //@IBOutlet weak var imageBtn: UIButton!
    
    var presenter: CreateExtraPresenter?
    var router: CreateExtraRouter?
    //var extraImage: UIImage?
    weak var delegate: CreateExtraDelegate?
    var extra: CreateExtraModel?
//    lazy var imagePicker: GalleryPickerHelper? = {
//        let picker = GalleryPickerHelper()
//        picker.onPickImage = { [weak self] image in
//            self?.imageBtn.setImage(image, for: .normal)
//            self?.extraImage = image
//        }
//        return picker
//    }()
}

// MARK: - ...  LifeCycle
extension CreateExtraVC {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension CreateExtraVC {
    func setup() {
    }
}
extension CreateExtraVC {
    override func backBtn(_ sender: Any) {
        router?.didCreate()
    }
    @IBAction func extraImage(_ sender: Any) {
        //imagePicker?.pick(in: self)
    }
    @IBAction func create(_ sender: Any) {
//        if extraImage == nil {
//            openConfirmation(title: "Please select the extra image".localized)
//            return
//        }
        if !validate(txfs: [nameTxf, priceTxf]) {
            return
        }
        extra = .init()
        extra?.name = nameTxf.text
        extra?.price = priceTxf.text?.double()
        //model.image = extraImage
        startLoading()
        presenter?.create(extra: extra)
    }
}
// MARK: - ...  View Contract
extension CreateExtraVC: CreateExtraViewContract, PopupConfirmationViewContract {
    func didCreate() {
        stopLoading()
        router?.didCreate(for: extra)
    }
    func didError(error: String?) {
        openConfirmation(title: error)
    }
}

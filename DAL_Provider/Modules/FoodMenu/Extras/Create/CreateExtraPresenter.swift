//
//  CreateExtraPresenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class CreateExtraPresenter: BasePresenter<CreateExtraViewContract> {
}
// MARK: - ...  Presenter Contract
extension CreateExtraPresenter: CreateExtraPresenterContract {
    func create(extra: CreateExtraModel?) {
        NetworkManager.instance.paramaters["name_ar"] = extra?.name
        NetworkManager.instance.paramaters["name_en"] = extra?.name
        NetworkManager.instance.paramaters["price"] = extra?.price
        NetworkManager.instance.request(.extra, type: .post, CreateExtraData.self) { [weak self] (response) in
           switch response {
               case .success(let model):
                self?.view?.didCreate()
               case .failure(let error):
                self?.view?.didError(error: error?.localizedDescription)
           }
       }
    }
}

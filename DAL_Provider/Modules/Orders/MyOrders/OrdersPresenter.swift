//
//  OrdersPresenter.swift
//  DAL_IOS
//
//  Created by M.abdu on 2/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class OrdersPresenter: BasePresenter<OrdersViewContract> {
}
// MARK: - ...  Presenter Contract
extension OrdersPresenter: OrdersPresenterContract {
    func fetchOrders(tab: OrdersVC.Tab) {
        if tab == .new {
            NetworkManager.instance.paramaters["type"] = "new"
        } else if tab == .processing {
            NetworkManager.instance.paramaters["type"] = "current"
        } else {
            NetworkManager.instance.paramaters["type"] = "finished"
        }
        //new, processing, finished, canceled
        NetworkManager.instance.request(.orders, type: .get, OrdersModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFetch(for: model?.data)
                case .failure:
                    self?.view?.stopLoading()

            }
        }
    }
    func filterOrder(filter: ReservationsFilterModel?) {
        NetworkManager.instance.paramaters["from"] = filter?.from
        NetworkManager.instance.paramaters["to"] = filter?.to
        NetworkManager.instance.paramaters["time"] = filter?.time?.rawValue
        NetworkManager.instance.paramaters["status"] = filter?.status
        //new, processing, finished, canceled
        NetworkManager.instance.request(.filterOrder, type: .post, OrdersModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFetch(for: model?.data)
                case .failure:
                    self?.view?.stopLoading()
            }
        }
    }
    func finishOrder(for id: Int?) {
        NetworkManager.instance.paramaters["order_id"] = id
        NetworkManager.instance.request(.finishOrder, type: .post, AcceptOrderModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFinishOrder(for: id)
                case .failure:
                    self?.view?.stopLoading()

            }
        }
    }
}
// MARK: - ...  Example of network response
extension OrdersPresenter {
}

//
//  OrderCell.swift
//  DAL_IOS
//
//  Created by M.abdu on 2/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import UIKit

protocol OrderCellDelegate: class {
    func orderCell(_ cell: OrderCell?, accept: Bool?)
    func orderCell(_ cell: OrderCell?, cancel: Bool?)
    func orderCell(_ cell: OrderCell?, finish: Bool?)
}
class OrderCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var mealsLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var newOrderView: UIView!
    @IBOutlet weak var processingView: UIView!
    @IBOutlet weak var timerBtn: UIButton!
    @IBOutlet weak var finishBtn: GradientButton!
    
    
    var tab: OrdersVC.Tab = .new
    weak var delegate: OrderCellDelegate?
    func setup() {
        guard let model = model as? OrdersModel.Datum? else { return }
        userImage.setImage(url: model?.userImg)
        usernameLbl.text = model?.userName
        dateLbl.text = model?.createdAt
        var extras = "\(model?.items?.first?.productName ?? "")"
        var counter = 1
        for extra in model?.items?.first?.extra ?? [] {
            if counter == model?.items?.first?.extra?.count {
                extras += "\(extra.extra?.name ?? ""),  "
            } else {
                extras += " \(extra.extra?.name ?? "")"
            }
            counter += 1
        }
        mealsLbl.text = "\("extras".localized): \(extras)"
        
        if tab == .new {
            newOrderView.isHidden = false
            processingView.isHidden = true
            statusLbl.text = "\("count".localized) \(model?.items?.count ?? 0) \("meal".localized)"
        } else if tab == .processing {
            newOrderView.isHidden = true
            processingView.isHidden = false
            statusLbl.text = "\("count".localized) \(model?.items?.count ?? 0) \("meal".localized)"
            timerBtn.setTitle("\("Remaining".localized) \(model?.remaining ?? "") \("Minute".localized)", for: .normal)
        } else {
            newOrderView.isHidden = true
            processingView.isHidden = true
            statusLbl.text = model?.statusText
            if model?.status == 3 {
                statusLbl.textColor = R.color.textColorBlue()
            } else if model?.status == 4 {
                statusLbl.textColor = R.color.thirdColor()
            }
        }
    }
    @IBAction func reject(_ sender: Any) {
        delegate?.orderCell(self, cancel: true)
    }
    @IBAction func accept(_ sender: Any) {
        delegate?.orderCell(self, accept: true)
    }
    @IBAction func finish(_ sender: Any) {
        delegate?.orderCell(self, finish: true)
    }
}

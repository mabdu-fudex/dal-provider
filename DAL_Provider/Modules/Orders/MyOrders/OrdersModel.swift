// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let ordersModel = try? newJSONDecoder().decode(OrdersModel.self, from: jsonData)

import Foundation

// MARK: - OrdersModel
struct OrdersModel: Codable {
    let status: Bool?
    let data: [Datum]?
    
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let userID: Int?
        let userName: String?
        let userImg: String?
        let userPhone: String?
        let providerID: Int?
        let providerName: String?
        let providerImg: String?
        let providerAddress: String?
        let status: Int?
        let createdAt, time, remaining, refuseal: String?
        //let addressID
        let notes: String?
        let subTotal, total: Int?
        let items: [Item]?
        let deliveryType: String?
        let service: Double?
        let tax: Double?
        let statusText: String?
        enum CodingKeys: String, CodingKey {
            case id
            case userID = "user_id"
            case userPhone = "user_phone"
            case userImg = "user_img"
            case userName = "user_name"
            case providerID = "provider_id"
            case providerName = "provider_name"
            case providerImg = "provider_img"
            case providerAddress = "provider_address"
            case status
            case createdAt = "created_at"
            case time, remaining, refuseal
            //case addressID = "address_id"
            case notes
            case subTotal = "sub_total"
            case total, items
            case deliveryType = "delivery_type"
            case statusText = "status_text"
            case service, tax
        }
    }

    // MARK: - Item
    struct Item: Codable {
        let id, userID, prodvierID, productID: Int?
        let orderID: Int?
        let productName: String?
        let productImg: String?
        let details: String?
        let qty: Int?
        let notes: String?
        let subTotal, total, status: Int?
        let createdAt, time: String?
        let extra: [ExtraElement]?

        enum CodingKeys: String, CodingKey {
            case id
            case userID = "user_id"
            case prodvierID = "prodvier_id"
            case productID = "product_id"
            case orderID = "order_id"
            case productName = "product_name"
            case productImg = "product_img"
            case details, qty, notes
            case subTotal = "sub_total"
            case total, status
            case createdAt = "created_at"
            case time, extra
        }
    }

    // MARK: - ExtraElement
    struct ExtraElement: Codable {
        let id, itemID, extraID, qty: Int?
        let createdAt, updatedAt: String?
        let extra: ExtraExtra?

        enum CodingKeys: String, CodingKey {
            case id
            case itemID = "item_id"
            case extraID = "extra_id"
            case qty
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case extra
        }
    }

    // MARK: - ExtraExtra
    struct ExtraExtra: Codable {
        let id, productID: Int?
        let nameAr, nameEn: String?
        var name: String? {
            if Localizer.current == .arabic {
                return nameAr
            } else {
                return nameEn
            }
        }
        let price: Int?
        let img: String?
        let createdAt, updatedAt: String?

        enum CodingKeys: String, CodingKey {
            case id
            case productID = "product_id"
            case nameAr = "name_ar"
            case nameEn = "name_en"
            case price, img
            case createdAt = "created_at"
            case updatedAt = "updated_at"
        }
    }

}

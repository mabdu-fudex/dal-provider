//
//  OrdersRouter.swift
//  DAL_IOS
//
//  Created by M.abdu on 2/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class OrdersRouter: Router {
    typealias PresentingView = OrdersVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension OrdersRouter: OrdersRouterContract {
//    func reOrder(for order: OrdersModel.Datum?) {
//        guard let scene = R.storyboard.reOrderStoryboard.reOrderVC() else { return }
//        scene.delegate = self
//        view?.pushPop(scene)
//    }
//    
    func order(for order: OrdersModel.Datum?) {
        guard let scene = R.storyboard.orderDetailsStoryboard.orderDetailsVC() else { return }
        scene.tab = view?.currentTab ?? .new
        scene.order = order
        view?.pushPop(scene)
    }
    func acceptOrder(for order: OrdersModel.Datum?) {
        guard let scene = R.storyboard.acceptOrderStoryboard.acceptOrderVC() else { return }
        scene.delegate = view
        scene.type = .order
        scene.order = order
        view?.pushPop(scene)
    }
    func rejectOrder(for order: OrdersModel.Datum?) {
        guard let scene = R.storyboard.rejectOrderStoryboard.rejectOrderVC() else { return }
        scene.delegate = view
        scene.type = .order
        scene.order = order
        view?.pushPop(scene)
    }
    func filter() {
        guard let scene = R.storyboard.reservationsFilterStoryboard.reservationsFilterVC() else { return }
        scene.delegate = view
        view?.pushPop(scene)
    }
}

//extension OrdersRouter: ReOrderDelegate {
//    func reOrder(_ delegate: ReOrderDelegate?, for order: OrdersModel.Datum?) {
//        view?.startLoading()
//        view?.presenter?.reOrder(for: order)
//
//    }
//}

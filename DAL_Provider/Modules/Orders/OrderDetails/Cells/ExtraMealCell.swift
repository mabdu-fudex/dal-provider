//
//  ExtraMealCell.swift
//  DAL_IOS
//
//  Created by M.abdu on 1/25/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import UIKit

protocol ExtraMealCellDelegate: class {
    func extraMealCell(for cell: ExtraMealCell)
}
class ExtraMealCell: UITableViewCell, CellProtocol {

    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    
    weak var delegate: ExtraMealCellDelegate?
    func setup() {
        guard let model = model as? OrdersModel.ExtraElement else { return }
        imageCell.setImage(url: model.extra?.img)
        titleLbl.text = "\(model.extra?.name ?? "")   (\(model.qty ?? 0))"
        let price = (model.extra?.price ?? 0).double * (model.qty ?? 0).double
        priceLbl.text = "\("SAR.short".localized) \(price)"
    }
    func setupExtraInOrder() {
        guard let model = model as? OrdersModel.ExtraElement else { return }
        imageCell.setImage(url: model.extra?.img)
        titleLbl.text = "\(model.extra?.name ?? "")   (\(model.qty ?? 0))"
        let price = (model.extra?.price ?? 0) * (model.qty ?? 0)
        priceLbl.text = "\("SAR.short".localized) \(price)"
    }
}

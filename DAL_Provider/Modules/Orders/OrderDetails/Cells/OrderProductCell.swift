//
//  OrderProductCell.swift
//  DAL_IOS
//
//  Created by Mabdu on 17/02/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import UIKit

class OrderProductCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var extrasTbl: UITableView!
    @IBOutlet weak var additionsLbl: UILabel!
    
    func setup() {
        guard let model = model as? OrdersModel.Item else { return }
        imageCell.setImage(url: model.productImg)
        titleLbl.text = "\(model.productName ?? "") X \(model.qty ?? 0)"
        descLbl.text = model.details
        priceLbl.text = model.total?.string
        extrasTbl.delegate = self
        extrasTbl.dataSource = self
        reloadHeight()
        if model.extra?.count == 0 {
            additionsLbl.isHidden = true
        }
    }
    
}
extension OrderProductCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let model = model as? OrdersModel.Item else { return 0}
        return model.extra?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = model as? OrdersModel.Item else { return UITableViewCell() }
        var cell = tableView.cell(type: ExtraMealCell.self, indexPath)
        cell.model = model.extra?[safe: indexPath.row]
        cell.setupExtraInOrder()
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        reloadHeight()
    }
    func reloadHeight() {
        guard let model = model as? OrdersModel.Item else { return }
        if let constraint = extrasTbl.constraints.first(where: { $0.firstAttribute == .height }) {
            constraint.constant = 55 * (model.extra?.count ?? 0).cgFloat
        }
    }
}

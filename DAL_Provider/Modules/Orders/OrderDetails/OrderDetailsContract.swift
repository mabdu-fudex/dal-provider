//
//  OrderDetailsContract.swift
//  DAL_IOS
//
//  Created by M.abdu on 2/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol OrderDetailsPresenterContract: PresenterProtocol {
     
}
// MARK: - ...  View Contract
protocol OrderDetailsViewContract: PresentingViewProtocol {

}
// MARK: - ...  Router Contract
protocol OrderDetailsRouterContract: Router, RouterProtocol {
    func dismiss()
    func acceptOrder(for order: OrdersModel.Datum?)
    func rejectOrder(for order: OrdersModel.Datum?)
}

protocol OrderDetailsDelegate: class {
    func orderDetails(_ delegate: OrderDetailsDelegate?)
}

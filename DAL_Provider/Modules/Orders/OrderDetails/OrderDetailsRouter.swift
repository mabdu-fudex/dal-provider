//
//  OrderDetailsRouter.swift
//  DAL_IOS
//
//  Created by M.abdu on 2/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class OrderDetailsRouter: Router {
    typealias PresentingView = OrderDetailsVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension OrderDetailsRouter: OrderDetailsRouterContract {
    func dismiss() {
        view?.delegate?.orderDetails(view?.delegate)
        view?.dismiss(animated: true, completion: nil)
    }
    func acceptOrder(for order: OrdersModel.Datum?) {
        guard let scene = R.storyboard.acceptOrderStoryboard.acceptOrderVC() else { return }
        scene.delegate = view
        scene.type = .order
        scene.order = order
        view?.pushPop(scene)
    }
    func rejectOrder(for order: OrdersModel.Datum?) {
        guard let scene = R.storyboard.rejectOrderStoryboard.rejectOrderVC() else { return }
        scene.delegate = view
        scene.type = .order
        scene.order = order
        view?.pushPop(scene)
    }
}

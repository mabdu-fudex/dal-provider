//
//  OrderDetailsVC.swift
//  DAL_IOS
//
//  Created by M.abdu on 2/11/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit
import Cosmos
// MARK: - ...  ViewController - Vars
class OrderDetailsVC: BaseController {
    @IBOutlet weak var centerTypeConstraint: NSLayoutConstraint!
    @IBOutlet weak var orderTypeLbl: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var deliveryLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var statusBtn: GradientButton!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var mealsTbl: UITableView!
    @IBOutlet weak var mealsPriceLbl: UILabel!
    @IBOutlet weak var extrasPriceLbl: UILabel!
    @IBOutlet weak var totalPriceLbl: UILabel!
    @IBOutlet weak var noteLbl: UILabel!
    @IBOutlet weak var newOrderView: UIView!
    @IBOutlet weak var rejectView: UIView!
    @IBOutlet weak var reasonLbl: UILabel!
    var presenter: OrderDetailsPresenter?
    var router: OrderDetailsRouter?
    var tab: OrdersVC.Tab = .new
    var order: OrdersModel.Datum?
    weak var delegate: OrderDetailsDelegate?
}

// MARK: - ...  LifeCycle
extension OrderDetailsVC {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
        setupUI()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension OrderDetailsVC {
    func setup() {
        if Localizer.current == .arabic {
            centerTypeConstraint.constant = 70
        } else {
            centerTypeConstraint.constant = -70
        }
        if tab == .new {
            setupUIForNewOrder()
        } else {
            setupUIForCompleteOrder()
        }
    }
    func setupUI() {
        orderTypeLbl.text = "\("Order type".localized)  \("Meal order".localized)"
        orderTypeLbl.attributedText = orderTypeLbl.text?.colorOfWord("\("Meal order".localized)", with: R.color.secondColor() ?? .black)
        noteLbl.text = order?.notes
        reasonLbl.text = order?.refuseal
        userImage.setImage(url: order?.userImg)
        usernameLbl.text = order?.userName
        dateLbl.text = "\(order?.createdAt ?? "") \(order?.time ?? "")"
        statusBtn.setTitle("\(order?.statusText ?? "")", for: .normal)
        
        if tab == .new {
            if order?.deliveryType == "indoor" {
                deliveryLbl.text = "The pickup will take place in the restaurant".localized
            } else {
                deliveryLbl.text = "The collection will take place at home".localized
            }
        } else {
            if order?.deliveryType == "indoor" {
                deliveryLbl.text = "Received from resturant".localized
            } else {
                deliveryLbl.text = "Received from home".localized
            }
        }
        if order?.status == 4 {
            rejectView.isHidden = false
        } else {
            rejectView.isHidden = true
        }
        ///mealsTbl.estimatedRowHeight = 150
        mealsTbl.delegate = self
        mealsTbl.dataSource = self
        mealsTbl.reloadData()
        mealsTbl.scrollToBottom()
        reloadHeight()
        setupPrices()
    }
    func setupPrices() {
        var mealPrice = 0.0
        var extraPrice = 0.0
        for meal in order?.items ?? [] {
            mealPrice += meal.total?.double ?? 0
            for extra in meal.extra ?? [] {
                extraPrice += (extra.qty ?? 0).double * (extra.extra?.price ?? 0).double
            }
        }
        taxLbl.text = "\("Includes value added".localized) \(order?.tax ?? 0)%"
        mealsPriceLbl.text = mealPrice.string
        extrasPriceLbl.text = extraPrice.string
        totalPriceLbl.text = order?.total?.double.string
    }
    func setupUIForNewOrder() {

    }
    func setupUIForCompleteOrder() {
        newOrderView.isHidden = true
        statusBtn.firstColor = R.color.textColorBlue() ?? .clear
        statusBtn.secondColor = R.color.textColorBlue() ?? .clear
        statusBtn.thirdColor = UIColor(red: 69/255, green: 212/255, blue: 207/255, alpha: 1)
    }
}
extension OrderDetailsVC {
    override func backBtn(_ sender: Any) {
        router?.dismiss()
    }
    @IBAction func accept(_ sender: Any) {
        router?.acceptOrder(for: order)
    }
    @IBAction func reject(_ sender: Any) {
        router?.rejectOrder(for: order)
    }
}
// MARK: - ...  View Contract
extension OrderDetailsVC: OrderDetailsViewContract, PopupConfirmationViewContract {

}

extension OrderDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order?.items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: OrderProductCell.self, indexPath)
        cell.model = order?.items?[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        reloadHeight()
    }
    func reloadHeight() {
        var height: CGFloat = 0
        var extras = 0
        for item in order?.items ?? [] {
            if item.extra?.count == 0 {
                height += 90
            } else {
                height += 130
            }
            extras += item.extra?.count ?? 0
        }
        height += (extras * 55).cgFloat
        if let constraint = mealsTbl.constraints.first(where: { $0.firstAttribute == .height }) {
            constraint.constant = height
        }
    }
}

extension OrderDetailsVC: AcceptOrderDelegate {
    func acceptOrder(_ delegate: AcceptOrderDelegate?, order: OrdersModel.Datum?) {
        router?.dismiss()
    }
}
extension OrderDetailsVC: RejectOrderDelegate {
    func rejectOrder(_ delegate: RejectOrderDelegate?, order: OrdersModel.Datum?) {
        router?.dismiss()
    }
}

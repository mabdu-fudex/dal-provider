//
//  EditProfileStep1VC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class EditProfileStep1VC: BaseController {
    @IBOutlet weak var nameEditTxf: EditTextFieldView!
    @IBOutlet weak var emailEditTxf: EditTextFieldView!
    @IBOutlet weak var mobileEditTxf: EditTextFieldView!
    @IBOutlet weak var addressEditTxf: EditTextFieldView!
    @IBOutlet weak var passwordEditTxf: EditTextFieldView!
    @IBOutlet weak var social1EditTxf: EditTextFieldView!
    @IBOutlet weak var social2EditTxf: EditTextFieldView!
    @IBOutlet weak var classificationCollection: EditTagsView!
    
    var presenter: EditProfileStep1Presenter?
    var router: EditProfileStep1Router?
    var mobile: String?
    var countryCode: String?
    var mainProvider: UserRoot.User? {
        return UserRoot.fetchUser()
    }
    var serviceTypes: [ServiceTypeModel.Datum] = []
    var paramters: [String: Any] = [:]
}

// MARK: - ...  LifeCycle
extension EditProfileStep1VC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
        setup()
        setupMainProvider()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        presenter?.fetchServiceTypes()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}
// MARK: - ...  Functions
extension EditProfileStep1VC {
    func setup() {
        passwordEditTxf.textTxf.textContentType = .oneTimeCode
        nameEditTxf.delegate = self
        emailEditTxf.delegate = self
        mobileEditTxf.delegate = self
        addressEditTxf.delegate = self
        social1EditTxf.delegate = self
        social2EditTxf.delegate = self
    }
    func setupMainProvider() {
        nameEditTxf.text = mainProvider?.attributes?.providerName
        emailEditTxf.text = mainProvider?.email
        mobileEditTxf.text = mainProvider?.phone
        addressEditTxf.text = mainProvider?.address
        social1EditTxf.text = mainProvider?.attributes?.socailAccount1
        social2EditTxf.text = mainProvider?.attributes?.socailAccount2
        var array: [ServiceTypeModel.Datum] = []
        var pathes: [Int] = []
        for item in mainProvider?.categories ?? [] {
            let model = ServiceTypeModel.Datum(id: item.categoryID, img: nil, name: item.category?.name, active: 1)
            array.append(model)
            for index in 0..<serviceTypes.count {
                if serviceTypes[index].id == model.id {
                    pathes.append(index)
                }
            }
        }
        classificationCollection.selectedItemsTags = array
        classificationCollection.selectedItemsPaths = pathes
        classificationCollection.reload()
        paramters["lat"] = mainProvider?.lat
        paramters["lng"] = mainProvider?.lng
        paramters["address"] = mainProvider?.address
    }
}
// MARK: - ...  Actions
extension EditProfileStep1VC {
    override func backBtn(_ sender: Any) {
        self.router?.profile()
    }
    @IBAction func locationProvider(_ sender: Any) {
        self.router?.goToStep2(withUser: mainProvider?.id ?? 0)
    }
    @IBAction func bankAccount(_ sender: Any) {
        router?.goToStep3(withUser: mainProvider?.id ?? 0)
    }
}
// MARK: - ...  View Contract
extension EditProfileStep1VC: EditProfileStep1ViewContract, PopupConfirmationViewContract {
    func didSuccessStep1(withUser id: Int) {
        //self.router?.goToStep2(withUser: id)
    }
    func didFailStep1(error: String?) {
        self.openConfirmation(title: error)
    }
    func didFetchServiceTypes(types: [ServiceTypeModel.Datum]) {
        serviceTypes.append(contentsOf: types)
        classificationCollection.delegate = self
        classificationCollection.dataSource = self
        setupMainProvider()
    }
}

extension EditProfileStep1VC: LocationFromMapDelegateContract {
    func saveLocation(lat: Double?, lng: Double?, address: String?) {
        paramters["lat"] = lat
        paramters["lng"] = lng
        paramters["address"] = address
        addressEditTxf.text = address
    }
    func didFailLocation() {
        
    }
}

extension EditProfileStep1VC: EditViewDelegate, TagCollectionViewDataSource {
    func tagCollectionView(for collection: TagCollectionView?) -> [TagModel] {
        return serviceTypes
    }
    func tagCollectionView(for collection: EditTagsView?) -> [TagModel] {
        return serviceTypes
    }
    func editView(didSave tagView: EditTagsView) {
        self.presenter?.updateStep1()
    }
    func editView(didSave view: EditTextFieldView) {
        self.presenter?.updateStep1()
    }
    func editView(didEdit view: EditTextFieldView) {
        if view == addressEditTxf {
            self.router?.openMap()
        }
    }
}

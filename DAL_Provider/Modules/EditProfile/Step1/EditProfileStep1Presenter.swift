//
//  EditProfileStep1Presenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class EditProfileStep1Presenter: BasePresenter<EditProfileStep1ViewContract> {
}
// MARK: - ...  Presenter Contract
extension EditProfileStep1Presenter: EditProfileStep1PresenterContract {
    func fetchServiceTypes() {
        NetworkManager.instance.request(.categories, type: .get, ServiceTypeModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFetchServiceTypes(types: model?.data ?? [])
                case .failure:
                    break
            }
        }
    }
    func updateStep1() {
        guard let view = view as? EditProfileStep1VC else { return }
        var paramters = view.paramters
        paramters["name"] = view.nameEditTxf.text
        paramters["email"] = view.emailEditTxf.text
        paramters["phone"] = view.mobileEditTxf.text
        if view.passwordEditTxf.text?.count ?? 0 > 0 {
            paramters["password"] = view.passwordEditTxf.text
        }
        paramters["socail_account1"] = view.social1EditTxf.text
        paramters["socail_account2"] = view.social2EditTxf.text
//        for index in 0..<view.classificationCollection.selectedItemsTags.count {
//            paramters["categories[\(index)]"] = view.classificationCollection.selectedItemsTags[index].id
//        }
        var ids: [Int] = []
        for index in 0..<view.classificationCollection.selectedItemsTags.count {
            ids.append(view.classificationCollection.selectedItemsTags[index].id ?? 0)
        }
        paramters["categories"] = ids.toJson()
        
        view.startLoading()
        NetworkManager.instance.paramaters = paramters
        NetworkManager.instance.request(.profile, type: .post, UserRoot.self) { [weak self] (response) in
            self?.view?.stopLoading()
            switch response {
                case .success(let model):
                    self?.view?.didSuccessStep1(withUser: model?.data?.id ?? 0)
                case .failure(let error):
                    self?.view?.didFailStep1(error: error?.localizedDescription)
            }
        }
    }
}

//
//  EditProfileStep1Router.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class EditProfileStep1Router: Router {
    typealias PresentingView = EditProfileStep1VC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension EditProfileStep1Router: EditProfileStep1RouterContract {
    func openMap() {
        guard let scene = R.storyboard.locationFromMapStoryboard.locationFromMapVC() else { return }
        scene.delegate = view
        self.view?.pushPop(scene)
    }
    func goToStep2(withUser id: Int) {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.050) {
            guard let scene = R.storyboard.editProfileStep2Storyboard.editProfileStep2VC() else { return }
            scene.userID = id
            if self.view?.mainProvider != nil {
                scene.mainProvider = self.view?.mainProvider
            }
            self.view?.push(scene)
        }
    }
    func goToStep3(withUser id: Int) {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.050) {
            guard let scene = R.storyboard.editProfileStep3Storyboard.editProfileStep3VC() else { return }
            self.view?.push(scene)
        }
    }
    func profile() {
        for controller in view?.navigationController?.viewControllers ?? [] {
            if controller is ProfileVC {
                view?.navigationController?.popToViewController(controller, animated: true)
            }
        }
    }
}

//
//  EditProfileStep1Contract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol EditProfileStep1PresenterContract: PresenterProtocol {
    func fetchServiceTypes()
    func updateStep1()
}
// MARK: - ...  View Contract
protocol EditProfileStep1ViewContract: PresentingViewProtocol {
    func didFetchServiceTypes(types: [ServiceTypeModel.Datum])
    func didSuccessStep1(withUser id: Int)
    func didFailStep1(error: String?)
}
// MARK: - ...  Router Contract
protocol EditProfileStep1RouterContract: Router, RouterProtocol {
    func openMap()
    func goToStep2(withUser id: Int)
    func goToStep3(withUser id: Int)
    func profile()
}

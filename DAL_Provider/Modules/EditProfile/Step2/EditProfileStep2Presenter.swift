//
//  EditProfileStep2Presenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class EditProfileStep2Presenter: BasePresenter<EditProfileStep2ViewContract> {
}
// MARK: - ...  Presenter Contract
extension EditProfileStep2Presenter: EditProfileStep2PresenterContract {
    func fetchFeatures() {
        NetworkManager.instance.request(.features, type: .get, ServiceTypeModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFetchFeatures(list: model?.data)
                case .failure:
                    break
            }
        }
    }
    func fetchNationality() {
        NetworkManager.instance.request(.nationalites, type: .get, ServiceTypeModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFetchNationality(list: model?.data)
                case .failure:
                    break
            }
        }
    }
    func registerStep2() {
        guard let view = view as? EditProfileStep2VC else { return }
        var paramters: [String: Any] = [:]
        paramters["user_id"] = view.userID
        paramters["provider_name"] = view.nameEditTxf.text
        paramters["nationality_id"] = view.nationalityCollection.selectedItemsTags.first?.id
        paramters["meat_type"] = view.meatTypeEditTxf.text
        paramters["meat_source"] = view.meatSourceCollection.selectedItemsTags.first?.name
        paramters["other_meat_source"] = view.externalMeatEditTxf.text
        
//        for index in 0..<view.servicesCollection.selectedItemsTags.count {
//            paramters["features[\(index)]"] = view.servicesCollection.selectedItemsTags[index].id
//        }
        var ids: [Int] = []
        for index in 0..<view.servicesCollection.selectedItemsTags.count {
            ids.append(view.servicesCollection.selectedItemsTags[index].id ?? 0)
        }
        paramters["features"] = ids.toJson()
        
        if case view.diffrenetSelectView.selectedItem = 1 {
            paramters["mix"] = "1"
        } else {
            paramters["mix"] = "0"
        }
        paramters["minimum_order_price"] = view.minimumDeliveryEditTxf.text
        paramters["free_order_price"] = view.freeDeliveryEditTxf.text
        paramters["work_hours"] = view.workHours
//        paramters["from"] = view.fromHour
//        paramters["to"] = view.toHour
        paramters["details_ar"] = view.aboutPlaceEditTxf.text
        paramters["details_en"] = view.aboutPlaceEditTxf.text
        if case view.deliverySelectView.selectedItem = 1 {
            paramters["delivery_option"] = "1"
        } else {
            paramters["delivery_option"] = "0"
        }
        if case view.reservationSelectView.selectedItem = 1 {
            paramters["table_option"] = "1"
        } else {
            paramters["table_option"] = "0"
        }
        if case view.taxSelectView.selectedItem = 1 {
            paramters["price_added"] = "1"
        } else {
            paramters["price_added"] = "0"
        }
        view.startLoading()
        NetworkManager.instance.paramaters = paramters
        let method = NetworkConfigration.EndPoint.endPoint(point: .providerRegisterStep2, paramters: [])
        NetworkManager.instance.uploadMultiImages(method, type: .post, files: view.images, key: "providerimages", file: ["avatar": view.avatarImage], UserRoot.self) { [weak self] (response) in
            self?.view?.stopLoading()
            switch response {
                case .success(let model):
                    model?.save()
                    self?.view?.didSuccessStep2()
                case .failure(let error):
                    self?.view?.didFailStep2(error: error?.localizedDescription)
            }
        }
    }
}

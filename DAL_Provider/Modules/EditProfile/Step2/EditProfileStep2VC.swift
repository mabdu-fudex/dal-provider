//
//  EditProfileStep2VC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class EditProfileStep2VC: BaseController {
    @IBOutlet weak var logoBtn: UIButton!
    @IBOutlet weak var nameEditTxf: EditTextFieldView!
    @IBOutlet weak var nationalityCollection: EditTagsView!
    @IBOutlet weak var servicesCollection: EditTagsView!
    @IBOutlet weak var diffrenetSelectView: EditSelectView!
    @IBOutlet weak var meatTypeEditTxf: EditTextFieldView!
    @IBOutlet weak var meatSourceCollection: EditTagsView!
    @IBOutlet weak var externalMeatEditTxf: EditTextFieldView!
    @IBOutlet weak var minimumDeliveryEditTxf: EditTextFieldView!
    @IBOutlet weak var freeDeliveryEditTxf: EditTextFieldView!
    @IBOutlet weak var deliverySelectView: EditSelectView!
    @IBOutlet weak var reservationSelectView: EditSelectView!
    @IBOutlet weak var taxSelectView: EditSelectView!
    @IBOutlet weak var workingSelectView: EditSelectView!
    @IBOutlet weak var fromTimeBtn: UIButton!
    @IBOutlet weak var toTimeBtn: UIButton!
    @IBOutlet weak var aboutPlaceEditTxf: EditTextFieldView!
    @IBOutlet weak var imagesView: UIView!
    @IBOutlet weak var imagesCollection: UICollectionView!
    
    
    var presenter: EditProfileStep2Presenter?
    var router: EditProfileStep2Router?
    var userID: Int?
    var mainProvider: UserRoot.User?
    var features: [ServiceTypeModel.Datum] = []
    var nationality: [ServiceTypeModel.Datum] = []
    var isDiff: Bool?
    var avatarImage: UIImage?
    var isDelivery: Bool?
    var isReserveTable: Bool?
    var isTax: Bool?
    var workingHours: [Int] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
    var fromHour: String?
    var toHour: String?
    var workHours: String?
    var images: [UIImage] = []
    var oldImages: [UIImageView] = []
    var editedImagePath: Int?
    lazy var imagePicker: GalleryPickerHelper? = {
        let picker = GalleryPickerHelper()
        picker.onPickImage = { [weak self] image in
            self?.logoBtn.setImage(image, for: .normal)
            self?.avatarImage = image
            self?.presenter?.registerStep2()
        }
        return picker
    }()
    lazy var providerImagesPicker: GalleryPickerHelper? = {
        let picker = GalleryPickerHelper()
        picker.onPickImage = { [weak self] image in
            if self?.editedImagePath != nil {
                self?.images[self?.editedImagePath ?? 0] = image
                self?.editedImagePath = nil
            } else {
                self?.images.append(image)
            }
            self?.imagesView.isHidden = false
            self?.imagesCollection.reloadData()
        }
        return picker
    }()
}

// MARK: - ...  LifeCycle
extension EditProfileStep2VC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
        fetch()
        if mainProvider != nil {
            setupMainProvider()
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupOldImages()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}
// MARK: - ...  Functions
extension EditProfileStep2VC {
    func fetch() {
        startLoading()
        presenter?.fetchNationality()
    }
    func reload() {
        nameEditTxf.delegate = self
        diffrenetSelectView.delegate = self
        meatTypeEditTxf.delegate = self
        externalMeatEditTxf.delegate = self
        minimumDeliveryEditTxf.delegate = self
        freeDeliveryEditTxf.delegate = self
        deliverySelectView.delegate = self
        reservationSelectView.delegate = self
        taxSelectView.delegate = self
        workingSelectView.delegate = self
        workingSelectView.source = workingHours
        aboutPlaceEditTxf.delegate = self
        
        nationalityCollection.delegate = self
        nationalityCollection.dataSource = self
        servicesCollection.delegate = self
        servicesCollection.dataSource = self
        meatSourceCollection.delegate = self
        meatSourceCollection.dataSource = self
        meatSourceCollection.onlyItemSelected = true
    }
    func setup() {
        imagesCollection.delegate = self
        imagesCollection.dataSource = self
        imagesView.isHidden = false
    }
    func setupOldImages() {
        var index = 0
        for image in self.mainProvider?.images ?? [] {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
            imageView.setImage(url: image.img) {
                if let item = imageView.image {
                    self.images.append(item)
                    self.imagesCollection.reloadData()
                }
            }
            index += 1
        }
    }
    func setupMainProvider() {
        logoBtn.setImage(url: mainProvider?.avatar)
        if mainProvider?.attributes?.mix == 1 {
            diffrenetSelectView.selectedItem = 1
            diffrenetSelectView.text = "YES".localized
        } else {
            diffrenetSelectView.text = "NO".localized
        }
        
        nameEditTxf.text = mainProvider?.name
        meatTypeEditTxf.text = mainProvider?.attributes?.meatType
        externalMeatEditTxf.text = mainProvider?.attributes?.otherMeatSource
        
        
        var arrayNationality: [ServiceTypeModel.Datum] = []
        var pathesNationality: [Int] = []
        let model = ServiceTypeModel.Datum(id: mainProvider?.attributes?.nationalityID, img: nil, name: mainProvider?.attributes?.nationality?.name, active: 1)
        arrayNationality.append(model)
        for index in 0..<nationality.count {
            if nationality[index].id == model.id {
                pathesNationality.append(index)
            }
        }
        nationalityCollection.selectedItemsTags = arrayNationality
        nationalityCollection.selectedItemsPaths = pathesNationality
        nationalityCollection.reload()
        
        if mainProvider?.attributes?.meatSource == "EXTERNAL" {
            meatSourceCollection.selectedItemsTags = [ServiceTypeModel.Datum(id: 0, img: nil, name: "EXTERNAL".localized, active: 0)]
            meatSourceCollection.selectedItemsPaths = [0]
            meatSourceCollection.reload()
        } else {
            meatSourceCollection.selectedItemsTags = [ServiceTypeModel.Datum(id: 1, img: nil, name: "INTERNAL".localized, active: 0)]
            meatSourceCollection.selectedItemsPaths = [1]
            meatSourceCollection.reload()
        }
        
        var arrayServices: [ServiceTypeModel.Datum] = []
        var pathesServices: [Int] = []
        for item in mainProvider?.features ?? [] {
            let model = ServiceTypeModel.Datum(id: item.featureID, img: nil, name: item.feature?.name, active: 1)
            arrayServices.append(model)
            for index in 0..<features.count {
                if features[index].id == model.id {
                    pathesServices.append(index)
                }
            }
        }
        servicesCollection.selectedItemsTags = arrayServices
        servicesCollection.selectedItemsPaths = pathesServices
        servicesCollection.reload()
        
        setupMainProviderStep3()
    }
    func setupMainProviderStep3() {
        if mainProvider?.attributes?.deliveryOption == 1 {
            deliverySelectView.selectedItem = 1
            deliverySelectView.text = "YES".localized
        } else {
            deliverySelectView.text = "NO".localized
        }
        if mainProvider?.attributes?.tableOption == 1 {
            reservationSelectView.selectedItem = 1
            reservationSelectView.text = "YES".localized
        } else {
            reservationSelectView.text = "NO".localized
        }
        if mainProvider?.attributes?.priceAdded == 1 {
            taxSelectView.selectedItem = 1
            taxSelectView.text = "YES".localized
        } else {
            taxSelectView.text = "NO".localized
        }
        
        minimumDeliveryEditTxf.text = mainProvider?.attributes?.minimumOrderPrice
        freeDeliveryEditTxf.text = mainProvider?.attributes?.freeOrderPrice
        workHours = mainProvider?.attributes?.workHours
        toHour = mainProvider?.attributes?.to
        fromHour = mainProvider?.attributes?.from
        fromTimeBtn.setTitle(workHours, for: .normal)
        toTimeBtn.setTitle(workHours, for: .normal)
        aboutPlaceEditTxf.text = mainProvider?.attributes?.detailsAr
        workingSelectView.text = workHours
    }
    
}
extension EditProfileStep2VC {
    override func backBtn(_ sender: Any) {
        router?.profile()
    }
    @IBAction func personalData(_ sender: Any) {
        router?.goToStep1(withUser: mainProvider?.id ?? 0)
    }
    @IBAction func bankAccount(_ sender: Any) {
        router?.goToStep3(withUser: mainProvider?.id ?? 0)
    }
    @IBAction func editLogo(_ sender: Any) {
        imagePicker?.pick(in: self)
    }
}

extension EditProfileStep2VC {
    @IBAction func workingHour(_ sender: Any) {
//        guard let scene = R.storyboard.pickerViewHelper.pickerController() else { return }
//        scene.source = workingHours
//        scene.titleClosure = { [weak self] row in
//            return self?.workingHours[row]
//        }
//        scene.didSelectClosure = { [weak self] row in
//            let item = self?.workingHours[row]
//            self?.workingHourBtn.setTitle(item, for: .normal)
//            self?.workHours = item
//        }
//        pushPop(scene)
    }
    @IBAction func fromTime(_ sender: Any) {
        guard let scene = R.storyboard.pickTimeController.pickTimeController() else { return }
        scene.type = .time
        scene.closureTime = { [weak self] date in
            let time = DateHelper().date(date: date, format: "HH:mm")
            self?.fromTimeBtn.setTitle(time, for: .normal)
            DateHelper.currentLocal = .english
            self?.fromHour = DateHelper().date(date: date, format: "HH:mm")
            DateHelper.currentLocal = nil
        }
        pushPop(scene)
    }
    @IBAction func toTime(_ sender: Any) {
        guard let scene = R.storyboard.pickTimeController.pickTimeController() else { return }
        scene.type = .time
        scene.closureTime = { [weak self] date in
            let time = DateHelper().date(date: date, format: "HH:mm")
            self?.toTimeBtn.setTitle(time, for: .normal)
            DateHelper.currentLocal = .english
            self?.toHour = DateHelper().date(date: date, format: "HH:mm")
            DateHelper.currentLocal = nil
        }
        pushPop(scene)
    }
    @IBAction func uploadPhotos(_ sender: Any) {
        if images.count >= 5 {
            return
        }
        providerImagesPicker?.pick(in: self)
    } 
}
// MARK: - ...  View Contract
extension EditProfileStep2VC: EditProfileStep2ViewContract, PopupConfirmationViewContract {
    func didFetchNationality(list: [ServiceTypeModel.Datum]?) {
        presenter?.fetchFeatures()
        nationality.append(contentsOf: list ?? [])
    }
    func didFetchFeatures(list: [ServiceTypeModel.Datum]?) {
        stopLoading()
        features.append(contentsOf: list ?? [])
        reload()
        setupMainProvider()
    }
    func didSuccessStep2() {
        //router?.goToStep3(withUser: userID ?? 0)
    }
    func didFailStep2(error: String?) {
        openConfirmation(title: error)
    }
}

extension EditProfileStep2VC: TagCollectionViewDataSource, EditViewDelegate {
    func tagCollectionView(for collection: TagCollectionView?) -> [TagModel] {
        if collection == servicesCollection {
            return features
        } else if collection == nationalityCollection {
            return nationality
        } else {
            let model = ServiceTypeModel.Datum(id: 0, img: nil, name: "EXTERNAL".localized, active: 1)
            let model1 = ServiceTypeModel.Datum(id: 1, img: nil, name: "INTERNAL".localized, active: 1)
            return [model, model1]
        }
    }
    func tagCollectionView(for collection: EditTagsView?) -> [TagModel] {
        if collection == servicesCollection {
            return features
        } else if collection == nationalityCollection {
            return nationality
        } else {
            let model = ServiceTypeModel.Datum(id: 0, img: nil, name: "EXTERNAL".localized, active: 1)
            let model1 = ServiceTypeModel.Datum(id: 1, img: nil, name: "INTERNAL".localized, active: 1)
            return [model, model1]
        }
    }
    func editView(didSave tagView: EditTagsView) {
        self.presenter?.registerStep2()
    }
    func editView(didSave view: EditTextFieldView) {
        self.presenter?.registerStep2()
    }
    func editView(didSave selectView: EditSelectView) {
    
        self.presenter?.registerStep2()
    }
}

// MARK: - ...  Collection view
extension EditProfileStep2VC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 60)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if images.count == 5 {
            return 5
        } else {
            return images.count + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = imagesCollection.cell(type: RequsetImageCell.self, indexPath)
        cell.delegate = self
        if indexPath.row == images.count {
            cell.canPlus = true
        } else {
            cell.image = images[safe: indexPath.row]
            cell.canPlus = false
        }
        cell.setup()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? RequsetImageCell
        if case cell?.canPlus = false {
            editImage(indexPath.row)
        }
    }
    func editImage(_ path: Int) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.view.tintColor = .darkGray
        let acceptAction = UIAlertAction(title: "Edit".localized, style: .default) { [weak self] (_) -> Void in
            self?.editedImagePath = path
            self?.didPlusImage()
        }
        alert.addAction(acceptAction)
        let deleteAction = UIAlertAction(title: "Delete".localized, style: .default) { [weak self] (_) -> Void in
            self?.images.remove(at: path)
            self?.imagesCollection.reloadData()
        }
        alert.addAction(deleteAction)
        
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}
// MARK: - ...  Image cell
extension EditProfileStep2VC: RequestImageCellDelegate {
    func didPlusImage() {
        if images.count >= 5 {
            return
        }
        providerImagesPicker?.pick(in: self)
    }
}

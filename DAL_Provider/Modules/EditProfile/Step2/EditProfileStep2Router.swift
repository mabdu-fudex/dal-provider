//
//  EditProfileStep2Router.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class EditProfileStep2Router: Router {
    typealias PresentingView = EditProfileStep2VC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension EditProfileStep2Router: EditProfileStep2RouterContract {
    func goToStep3(withUser id: Int) {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.050) {
            guard let scene = R.storyboard.editProfileStep3Storyboard.editProfileStep3VC() else { return }
            self.view?.push(scene)
        }
    }
    func goToStep1(withUser id: Int) {
        guard let scene = R.storyboard.editProfileStep1Storyboard.editProfileStep1VC() else { return }
        view?.push(scene)
    }
    func profile() {
        for controller in view?.navigationController?.viewControllers ?? [] {
            if controller is ProfileVC {
                view?.navigationController?.popToViewController(controller, animated: true)
            }
        }
    }
}

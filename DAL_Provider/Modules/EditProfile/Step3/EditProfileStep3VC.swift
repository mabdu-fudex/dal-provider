//
//  EditProfileStep3VC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class EditProfileStep3VC: BaseController {
    @IBOutlet weak var banksTbl: UITableView!
    var presenter: EditProfileStep3Presenter?
    var router: EditProfileStep3Router?
    var banks: [EditProfileStep3Model.Datum] = []
}

// MARK: - ...  LifeCycle
extension EditProfileStep3VC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension EditProfileStep3VC {
    func setup() {
        banksTbl.delegate = self
        banksTbl.dataSource = self
        startLoading()
        presenter?.fetchBanks()
    }
    func reload() {
        if let constraint = banksTbl.constraints.first(where: { $0.firstAttribute == .height }) {
            constraint.constant = banksTbl.contentSize.height
        }
    }
    @IBAction func plus(_ sender: Any) {
        router?.createBank()
    }
    override func backBtn(_ sender: Any) {
        router?.profile()
    }
}
// MARK: - ...  View Contract
extension EditProfileStep3VC: EditProfileStep3ViewContract {
    func didFetchBanks(for list: [EditProfileStep3Model.Datum]?) {
        stopLoading()
        banks.removeAll()
        banks.append(contentsOf: list ?? [])
        banksTbl.reloadData {
            self.banksTbl.scrollToBottom()
        }
    }
}
extension EditProfileStep3VC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return banks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: BankTableCell.self, indexPath)
        cell.model = banks[indexPath.row]
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        reload()
    }
}

extension EditProfileStep3VC: BankTableCellDelegate {
    func bankTableCell(didEditCell cell: BankTableCell) {
        let bank = banks[safe: cell.path ?? 0]
        router?.createBank(for: bank)
    }
}

extension EditProfileStep3VC: CreateBankDelegate {
    func createBank(didCreate bank: EditProfileStep3Model.Datum?) {
        
    }
    
    func createBank(didEdit bank: EditProfileStep3Model.Datum?) {
        
    }
}

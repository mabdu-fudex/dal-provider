//
//  EditProfileStep3Contract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol EditProfileStep3PresenterContract: PresenterProtocol {
    func fetchBanks()
}
// MARK: - ...  View Contract
protocol EditProfileStep3ViewContract: PresentingViewProtocol {
    func didFetchBanks(for list: [EditProfileStep3Model.Datum]?)
}
// MARK: - ...  Router Contract
protocol EditProfileStep3RouterContract: Router, RouterProtocol {
    func createBank(for bank: EditProfileStep3Model.Datum?)
    func profile()
}

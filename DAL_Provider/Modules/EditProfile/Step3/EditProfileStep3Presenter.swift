//
//  EditProfileStep3Presenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class EditProfileStep3Presenter: BasePresenter<EditProfileStep3ViewContract> {
}
// MARK: - ...  Presenter Contract
extension EditProfileStep3Presenter: EditProfileStep3PresenterContract {
    func fetchBanks() {
        NetworkManager.instance.request(.userBanks, type: .get, EditProfileStep3Model.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didFetchBanks(for: model?.data)
                case .failure:
                    break
            }
        }
    }
}

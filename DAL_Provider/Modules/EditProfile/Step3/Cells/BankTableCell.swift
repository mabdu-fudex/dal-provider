//
//  BankTableCell.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/18/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import UIKit

protocol BankTableCellDelegate: class {
    func bankTableCell(didEditCell cell: BankTableCell)
}
class BankTableCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var bankNameLbl: UILabel!
    @IBOutlet weak var ibanLbl: UILabel!
    
    weak var delegate: BankTableCellDelegate?
    func setup() {
        guard let model = model as? EditProfileStep3Model.Datum else { return }
        bankNameLbl.text = model.bankName
        var text = ""
        for _ in 0..<(model.transferNumber?.count ?? 0) {
            text += "*"
        }
        ibanLbl.text = text
    }
    @IBAction func edit(_ sender: Any) {
        delegate?.bankTableCell(didEditCell: self)
    }
}

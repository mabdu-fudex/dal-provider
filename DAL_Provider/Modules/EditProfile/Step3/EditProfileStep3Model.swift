// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let banksModel = try? newJSONDecoder().decode(BanksModel.self, from: jsonData)

import Foundation

// MARK: - BanksModel
struct EditProfileStep3Model: Codable {
    let status: Bool?
    let data: [Datum]?
    // MARK: - Datum
    struct Datum: Codable {
        let id, userID: Int?
        let bankName, transferNumber, createdAt, updatedAt: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case userID = "user_id"
            case bankName = "bank_name"
            case transferNumber = "transfer_number"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
        }
    }

}


//
//  CreateBankAccountVC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/18/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class CreateBankAccountVC: BaseController {
    @IBOutlet weak var bankNameTxf: UITextField!
    @IBOutlet weak var ibanTxf: UITextField!
    @IBOutlet weak var createBtn: GradientButton!
    var presenter: CreateBankAccountPresenter?
    var router: CreateBankAccountRouter?
    var oldBank: EditProfileStep3Model.Datum?
    weak var delegate: CreateBankDelegate?
}

// MARK: - ...  LifeCycle
extension CreateBankAccountVC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension CreateBankAccountVC {
    func setup() {
        if oldBank != nil {
            bankNameTxf.text = oldBank?.bankName
            ibanTxf.text = oldBank?.transferNumber
            createBtn.setTitle("EDIT".localized, for: .normal)
        }
    }
    @IBAction func create(_ sender: Any) {
        if !validate(txfs: [bankNameTxf, ibanTxf]) {
            return
        }
        startLoading()
        presenter?.createBank(name: bankNameTxf.text, iban: ibanTxf.text, id: oldBank?.id)
    }
}
// MARK: - ...  View Contract
extension CreateBankAccountVC: CreateBankAccountViewContract {
    func didCreate(for bank: EditProfileStep3Model.Datum?) {
        if oldBank == nil {
            delegate?.createBank(didCreate: bank)
        } else {
            delegate?.createBank(didEdit: bank)
        }
        router?.bankList()
    }
}

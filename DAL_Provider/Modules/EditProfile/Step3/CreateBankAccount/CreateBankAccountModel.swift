//
//  CreateBankAccountModel.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/18/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Entity
class CreateBankAccountModel: Codable {
    let status: Bool?
    let data: EditProfileStep3Model.Datum?
}

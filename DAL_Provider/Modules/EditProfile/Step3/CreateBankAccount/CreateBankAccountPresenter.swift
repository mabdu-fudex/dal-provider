//
//  CreateBankAccountPresenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/18/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class CreateBankAccountPresenter: BasePresenter<CreateBankAccountViewContract> {
}
// MARK: - ...  Presenter Contract
extension CreateBankAccountPresenter: CreateBankAccountPresenterContract {
    func createBank(name: String?, iban: String?, id: Int?) {
        var paramters: [String: Any] = [:]
        paramters["bank_name"] = name
        paramters["transfer_number"] = iban
        NetworkManager.instance.paramaters = paramters
        var method = ""
        if id != nil {
            method = NetworkConfigration.EndPoint.endPoint(point: .bank, paramters: [id ?? 0])
        } else {
            method = NetworkConfigration.EndPoint.endPoint(point: .bank, paramters: [])
        }
        NetworkManager.instance.request(method, type: .post, CreateBankAccountModel.self) { [weak self] (response) in
            switch response {
                case .success(let model):
                    self?.view?.didCreate(for: model?.data)
                case .failure:
                    break
            }
        }
    }
}
// MARK: - ...  Example of network response
extension CreateBankAccountPresenter {
    func fetchResponse<T: CreateBankAccountModel>(response: NetworkResponse<T>) {
        switch response {
            case .success(_):
                break
            case .failure(_):
                break
        }
    }
}

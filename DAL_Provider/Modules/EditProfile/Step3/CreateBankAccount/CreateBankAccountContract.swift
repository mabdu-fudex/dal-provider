//
//  CreateBankAccountContract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/18/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol CreateBankAccountPresenterContract: PresenterProtocol {
    func createBank(name: String?, iban: String?, id: Int?)
}
// MARK: - ...  View Contract
protocol CreateBankAccountViewContract: PresentingViewProtocol {
    func didCreate(for bank: EditProfileStep3Model.Datum?)
}
// MARK: - ...  Router Contract
protocol CreateBankAccountRouterContract: Router, RouterProtocol {
    func bankList()
}

protocol CreateBankDelegate: class {
    func createBank(didCreate bank: EditProfileStep3Model.Datum?)
    func createBank(didEdit bank: EditProfileStep3Model.Datum?)
}

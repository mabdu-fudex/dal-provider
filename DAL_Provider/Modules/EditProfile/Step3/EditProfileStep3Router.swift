//
//  EditProfileStep3Router.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class EditProfileStep3Router: Router {
    typealias PresentingView = EditProfileStep3VC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension EditProfileStep3Router: EditProfileStep3RouterContract {
    func createBank(for bank: EditProfileStep3Model.Datum? = nil) {
        guard let scene = R.storyboard.createBankAccountStoryboard.createBankAccountVC() else { return }
        scene.delegate = view
        scene.oldBank = bank
        view?.push(scene)
    }
    func profile() {
        for controller in view?.navigationController?.viewControllers ?? [] {
            if controller is ProfileVC {
                view?.navigationController?.popToViewController(controller, animated: true)
            }
        }
    }
}

//
//  ProfileVC.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class ProfileVC: BaseController {
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileTitleLbl: UILabel!
    @IBOutlet weak var rateLbl: UILabel!
    var presenter: ProfilePresenter?
    var router: ProfileRouter?
   

}

// MARK: - ...  LifeCycle
extension ProfileVC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension ProfileVC {
    func setup() {
        profileTitleLbl.text = UserRoot.fetchUser()?.name
        profileImage.setImage(url: UserRoot.fetchUser()?.avatar)
        rateLbl.text = (UserRoot.fetchUser()?.rating?.double ?? 0).string
    }
}
extension ProfileVC {
    @IBAction func edit(_ sender: Any) {
        router?.editProfile()
    }
    @IBAction func foodMenu(_ sender: Any) {
        router?.foodMenu()
    }
    @IBAction func extraMenu(_ sender: Any) {
        router?.extras()
    }
    @IBAction func logout(_ sender: Any) {
        router?.logout()
    }
}
// MARK: - ...  View Contract
extension ProfileVC: ProfileViewContract {
}

//
//  ProfileRouter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class ProfileRouter: Router {
    typealias PresentingView = ProfileVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension ProfileRouter: ProfileRouterContract {
    func editProfile() {
        guard let scene = R.storyboard.editProfileStep1Storyboard.editProfileStep1VC() else { return }
        view?.push(scene)
    }
    
    func foodMenu() {
        guard let scene = R.storyboard.subCategoriesStoryboard.subCategoriesVC() else { return }
        view?.push(scene)
    }
    
    func logout() {
        UserRoot.save(response: Data())
        unAuthorized()
    }
    func extras() {
        guard let scene = R.storyboard.extrasListStoryboard.extrasListVC() else { return }
        view?.push(scene)
    }
}

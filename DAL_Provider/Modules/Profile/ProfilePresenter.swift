//
//  ProfilePresenter.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class ProfilePresenter: BasePresenter<ProfileViewContract> {
}
// MARK: - ...  Presenter Contract
extension ProfilePresenter: ProfilePresenterContract {
}
// MARK: - ...  Example of network response
extension ProfilePresenter {
    func fetchResponse<T: ProfileModel>(response: NetworkResponse<T>) {
        switch response {
            case .success(_):
                break
            case .failure(_):
                break
        }
    }
}

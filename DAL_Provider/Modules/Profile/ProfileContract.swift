//
//  ProfileContract.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol ProfilePresenterContract: PresenterProtocol {
}
// MARK: - ...  View Contract
protocol ProfileViewContract: PresentingViewProtocol {
}
// MARK: - ...  Router Contract
protocol ProfileRouterContract: Router, RouterProtocol {
    func editProfile()
    func foodMenu()
    func logout()
    func extras()
}

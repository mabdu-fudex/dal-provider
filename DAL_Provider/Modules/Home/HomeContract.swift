//
//  HomeContract.swift
//  DAL_IOS
//
//  Created by M.abdu on 12/29/20.
//  Copyright © 2020 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter Contract
protocol HomePresenterContract: PresenterProtocol {
    func fetchAds()
    func fetchUser()
}
// MARK: - ...  View Contract
protocol HomeViewContract: PresentingViewProtocol {
    func didFetch(for list: [AdsModel.Datum]?)
    func didFetchUser()
}
// MARK: - ...  Router Contract
protocol HomeRouterContract: Router, RouterProtocol {
    func notifications()
    func reservations()
    func orders()
    func profile()
}

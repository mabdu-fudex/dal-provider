//
//  HomeModel.swift
//  DAL_IOS
//
//  Created by M.abdu on 12/29/20.
//  Copyright © 2020 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Entity
class HomeModel: Codable {
}

// MARK: - AdsModel
struct AdsModel: Codable, Cached {
    let status: Bool?
    let data: [Datum]?
    
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let titleAr, titleEn, detailsAr, detailsEn: String?
        let img: String?
        let link: String?
        let active, sort, userID: Int?
        let createdAt, updatedAt: String?

        enum CodingKeys: String, CodingKey {
            case id
            case titleAr = "title_ar"
            case titleEn = "title_en"
            case detailsAr = "details_ar"
            case detailsEn = "details_en"
            case img, link, active, sort
            case userID = "user_id"
            case createdAt = "created_at"
            case updatedAt = "updated_at"
        }
    }

}

//
//  AdCell.swift
//  DAL_Provider
//
//  Created by Mabdu on 25/02/2021.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import UIKit

class AdCell: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var imageCell: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setup() {
        guard let model = model as? AdsModel.Datum else { return }
        imageCell.setImage(url: model.img)
        
        imageCell.UIViewAction {
            Common().openUrl(text: model.link)
        }
    }
}

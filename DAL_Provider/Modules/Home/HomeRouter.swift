//
//  HomeRouter.swift
//  DAL_IOS
//
//  Created by M.abdu on 12/29/20.
//  Copyright © 2020 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Router
class HomeRouter: Router {
    typealias PresentingView = HomeVC
    weak var view: PresentingView?
    deinit {
        self.view = nil
    }
}

extension HomeRouter: HomeRouterContract {
    func notifications() {
        guard let scene = R.storyboard.notificationsStoryboard.notificationsVC() else { return }
        view?.push(scene)
    }
    
    func reservations() {
        guard let scene = R.storyboard.reservationsStoryboard.reservationsVC() else { return }
        view?.push(scene)
    }
    
    func orders() {
        guard let scene = R.storyboard.ordersStoryboard.ordersVC() else { return }
        view?.push(scene)
    }
    
    func profile() {
        guard let scene = R.storyboard.profileStoryboard.profileVC() else { return }
        view?.push(scene)
    }
    
}

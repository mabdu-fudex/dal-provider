//
//  HomePresenter.swift
//  DAL_IOS
//
//  Created by M.abdu on 12/29/20.
//  Copyright © 2020 com.M.Abdu. All rights reserved.
//

import Foundation

// MARK: - ...  Presenter
class HomePresenter: BasePresenter<HomeViewContract> {
}
// MARK: - ...  Presenter Contract
extension HomePresenter: HomePresenterContract {
    func fetchAds() {
        NetworkManager.instance.request(.subCategories, type: .get, AdsModel.self) { [weak self] (response) in
           switch response {
               case .success(let model):
                model?.save()
                self?.view?.didFetch(for: model?.data)
               case .failure:
                   break
           }
       }
    }
    func fetchUser() {
        if UserRoot.token() == nil {
            return
        }
        NetworkManager.instance.paramaters["user_id"] = UserRoot.fetchUser()?.id
        NetworkManager.instance.request(.profile, type: .get, UserRoot.self) { [weak self] (response) in
           switch response {
               case .success(let model):
                model?.save()
                self?.view?.didFetchUser()
               case .failure:
                   break
           }
       }
    }
}

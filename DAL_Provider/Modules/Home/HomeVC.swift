//
//  HomeVC.swift
//  DAL_IOS
//
//  Created by M.abdu on 12/29/20.
//  Copyright © 2020 com.M.Abdu. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ...  ViewController - Vars
class HomeVC: BaseController {
    @IBOutlet weak var adsCollection: UICollectionView!
    @IBOutlet weak var providerImage: UIImageView!
    @IBOutlet weak var providerNameLbl: UILabel!
    @IBOutlet weak var reservationCountLbl: UILabel!
    @IBOutlet weak var ordersCountLbl: UILabel!
    var presenter: HomePresenter?
    var router: HomeRouter?
    var ads: [AdsModel.Datum] = []
}

// MARK: - ...  LifeCycle
extension HomeVC {
    override func viewDidLoad() {
        super.hideNav = true
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter = .init()
        presenter?.view = self
        router = .init()
        router?.view = self
        setup()
        presenter?.fetchUser()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter = nil
        router = nil
    }
}
// MARK: - ...  Functions
extension HomeVC {
    func setup() {
        providerNameLbl.text = UserRoot.fetchUser()?.name
        providerImage.setImage(url: UserRoot.fetchUser()?.avatar)
        ads = AdsModel.fetch(for: AdsModel.self)?.data ?? []
        adsCollection.delegate = self
        adsCollection.dataSource = self
        adsCollection.autoScrolling()
        presenter?.fetchAds()
    }
}
extension HomeVC {
    @IBAction func notifications(_ sender: Any) {
        router?.notifications()
    }
    @IBAction func reservations(_ sender: Any) {
        router?.reservations()
    }
    @IBAction func orders(_ sender: Any) {
        router?.orders()
    }
    @IBAction func profile(_ sender: Any) {
        router?.profile()
    }
}
// MARK: - ...  View Contract
extension HomeVC: HomeViewContract {
    func didFetch(for list: [AdsModel.Datum]?) {
        ads.removeAll()
        ads.append(contentsOf: list ?? [])
        adsCollection.reloadData()
    }
    func didFetchUser() {
        ordersCountLbl.text = "\(UserRoot.fetchUser()?.ordersCount ?? 0) \("NEW".localized)"
        reservationCountLbl.text = "\(UserRoot.fetchUser()?.reservationsCount ?? 0) \("NEW".localized)"
    }
}
extension HomeVC: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width - 40, height: 183)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ads.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.cell(type: AdCell.self, indexPath)
        cell.model = ads[indexPath.row]
        return cell
    }
}

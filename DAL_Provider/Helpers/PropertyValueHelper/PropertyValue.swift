//
//  PropertyValue.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

// The easiest way to persist data on iOS
// is through UserDefaults.
// UserDefaults is a key-value store. So having
// a nice type-safe wrapper arround it is pretty
// useful.
// There have been ways to do so since the beginning
// of Swift.
// 👍 Very clean call sites
// 👎 Requires boilerplate
// Perfect use case for a Property Wrapper!
@propertyWrapper
struct UserDefault<T> {
    let key: String
    let defaultValue: T
    
    init(_ key: String, defaultValue: T) {
        self.key = key
        self.defaultValue = defaultValue
    }
    
    var wrappedValue: T {
        get {
            return UserDefaults.standard.object(forKey: key) as? T ?? defaultValue
        }
        set {
            UserDefaults.standard.set(newValue, forKey: key)
        }
    }
    
    var projectedValue: Self {
        return self
    }
    
    func delete() {
        UserDefaults.standard.removeObject(forKey: key)
    }
}

struct UserDefaultsConfig {
   
}

@discardableResult
func discard(message: @autoclosure () -> String) -> Bool {
    return true
}

extension Optional where Wrapped == String {
    var orEmpty: String {
        switch self {
            case .some(let value):
                return value
            case .none:
                return ""
        }
    }
}

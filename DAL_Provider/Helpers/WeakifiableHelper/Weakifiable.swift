//
//  Weakifiable.swift
//  DAL_Provider
//
//  Created by M.abdu on 1/7/21.
//  Copyright © 2021 com.M.Abdu. All rights reserved.
//

import Foundation

protocol Weakifiable: class {
    
}

extension NSObject: Weakifiable {
    
}

extension Weakifiable {
    func weak<T>(_ handler: @escaping (Self, T) -> Void) -> (T) -> Void {
        return {
            [weak self] (data) in
            guard let self = self else { return }
            handler(self, data)
        }
    }
    func weak(_ handler: @escaping (Self) -> Void) -> () -> Void {
        return {
            [weak self] in
            guard let self = self else { return }
            handler(self)
        }
    }
}

//
//  EndPoint.swift
//  SupportI
//
//  Created by Mohamed Abdu on 3/20/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import Foundation
import UIKit
// MARK: - ...  Network layer configration
struct NetworkConfigration {
    static let URL = "https://dev.fudexsb.com/demo/dal/public/api/v1/"
    static let VERSION = "v1"
    static var useAuth: Bool = false
    // MARK: - ...  The Endpoints
    public enum EndPoint: String {
        case point
        case token
        case login
        case resendCode
        case sendCode
        case checkCode
        case register = "user/register"
        case updatePassword
        case profile
        case categories
        case providerRegisterStep1 = "provider/register/1"
        case providerRegisterStep2 = "provider/register/2"
        case providerRegisterStep3 = "provider/register/3"
        case nationalites
        case features
        case uploadReceipt
        case allProviders
        case subCategories = "userSubcategoryies"
        case createSubCategory = "subcategory"
        case allSubCategories = "subcategories"
        case deleteSubCategory = "subCategory"
        case myProducts
        case product
        case hideProduct = "product/active"
        case hideMenu = "subCategory/active"
        case userBanks
        case bank
        case ads
        case orders = "provider/orders"
        case reservations = "provider/reservations"
        case acceptReservation
        case refuseReservation
        case finishOrder
        case finishReservation
        case acceptOrder
        case refuseOrder
        case filterOrder
        case settings
        case questions
        case contact
        case extras
        case extra
    }
}

extension NetworkConfigration.EndPoint {
    static func endPoint(point: NetworkConfigration.EndPoint, paramters: [Any]) -> String {
        let method = NetworkManager.instance.slugs(point, paramters)
        return method
    }
}

